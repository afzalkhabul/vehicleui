/**
 * Created by Nibble on 4/22/2016.
 */

var zlib = require('zlib');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var stream = require('stream');

var routes = require('./routes/index');
var users = require('./routes/users');

//var _ = require('underscore');
var request = require('request');
var uuid = require('node-uuid');

var app = express();
var router = express.Router();
app.use(cookieParser());
app.use(function (req, res, next) {

    var urlString = req.url.toString();
    if (urlString.indexOf('api') > -1) {
        //console.log(req.url);
        var url ={};
        //console.log('details'+ req.headers['access_token']);
        if(urlString.indexOf('admin') > -1){
                var customURL=req.url;
            var requestData = customURL.replace("/admin/api", "/api");
            if(urlString.indexOf('?')>-1){
                url="http://localhost:3012" + requestData+"&access_token="+req.headers['access_token'];
                // url="http://139.162.27.78:3012" + requestData+"&access_token="+req.headers['access_token'];
            }else{
                url="http://localhost:3012" + requestData+"?access_token="+req.headers['access_token'];
                // url="http://139.162.27.78:3012" + requestData+"?access_token="+req.headers['access_token'];
             }
        }else if(urlString.indexOf('projectLegal') > -1) {
            var customURL = req.url;
            var requestData = customURL.replace("/projectLegal/api", "/api");
            console.log('url data'+requestData);
            if(urlString.indexOf('?')>-1){
					url="http://localhost:3012" + requestData+"&access_token="+req.headers['access_token'];
                // url="http://139.162.27.78:3012" + requestData+"&access_token="+req.headers['access_token'];
            }else{
            // url="http://139.162.27.78:3012" +requestData+"?access_token="+req.headers['access_token'];
              url="http://localhost:3012" +requestData+"?access_token="+req.headers['access_token'];
            }
        }  else{
            if(urlString.indexOf('filter')>-1){
               // url="http://139.162.27.78:3012" + req.url+"&access_token="+req.headers['access_token'];


                url="http://localhost:3012" + req.url+"&access_token="+req.headers['access_token'];

            }else{
                // url="http://139.162.27.78:3012" + req.url+"?access_token="+req.headers['access_token'];
                url="http://localhost:3012" + req.url+"?access_token="+req.headers['access_token'];

            }
            console.log('url details'+url);

        }
        var options = {
            url: url,
            headers: {
                'access_token': req.headers['access_token']
            }
        };
        req.pipe(request(options)).pipe(res);

    } else if(urlString.indexOf('logout') > -1) {
        req.clearCookie('JSESSIONID');
        res.clearCookie('JSESSIONID');
        next();
    } else {

        next();
    }
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

app.use('/verify', function(req, res, next){
    res.render('verify', {'title':'email'});
});
app.use('/home', function (req, res, next) {
    res.render('home', {title: 'Express'});
});

app.use('/users', users);
/*

app.use('/forgotPassword', function (req, res, next) {
    res.render('resetPassword', {title: 'ResetPassword'});
});


app.use('/payUCallBack', function (req, res, next) {
    console.log("request body"+JSON.stringify(req.body));
    console.log("res body"+res.body);
    console.log("res body"+res.body);
    res.render('payUCallBack', req.body);
});

app.use('/payUCallBackFailure', function (req, res, next) {
    console.log("request body"+JSON.stringify(req.body));
    console.log("res body"+res.body);
    res.render('payUCallBackFailure', req.body);
});
*/

app.use('/error', function (req, res, next) {
    res.set('Content-Type', 'text/html');
    fs.createReadStream('errorPage.html').pipe(res);
    //res.render('', {title: 'Dhanbad'});
});
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;

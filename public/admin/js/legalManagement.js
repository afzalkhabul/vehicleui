
// var HearingURL = 'http://139.162.27.78:8869:5757/admin/#/hearingDetails/';
var HearingURL = 'http://localhost:3008/admin/#/hearingDetails/';
//
// var uploadFileURL='http://139.162.27.78:8869/api/Uploads/dhanbadDb/download/';
var uploadFileURL='http://localhost:3008/api/Uploads/dhanbadDb/download/';



angular.module('ui.tinymce', ['ui.calendar'])
    .value('uiTinymceConfig', {})
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
        uiTinymceConfig = uiTinymceConfig || {};
        var generatedIds = 0;
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ngModel) {
                var expression, options, tinyInstance;
                // generate an ID if not present
                if (!attrs.id) {
                    attrs.$set('id', 'uiTinymce' + generatedIds++);
                }
                options = {
                    // Update model when calling setContent (such as from the source editor popup)
                    setup: function(ed) {
                        ed.on('init', function(args) {
                            ngModel.$render();
                        });
                        // Update model on button click
                        ed.on('ExecCommand', function(e) {
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                        // Update model on keypress
                        ed.on('KeyUp', function(e) {
                            console.log(ed.isDirty());
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                    },
                    mode: 'exact',
                    elements: attrs.id,
                };
                if (attrs.uiTinymce) {
                    expression = scope.$eval(attrs.uiTinymce);
                } else {
                    expression = {};
                }
                angular.extend(options, uiTinymceConfig, expression);
                setTimeout(function() {
                    tinymce.init(options);
                });
                ngModel.$render = function() {
                    if (!tinyInstance) {
                        tinyInstance = tinymce.get(attrs.id);
                    }
                    if (tinyInstance) {
                        tinyInstance.setContent(ngModel.$viewValue || '');
                    }
                };
            }
        };
    }]);
/*angular.module('calendarDemoApp', ['ui.calendar', 'ui.bootstrap']);*/

var app = angular.module('legalMgmt', ['ngRoute', 'ui.calendar', 'ngCalendar', 'ngFileUpload', 'datatables', 'angularjs-dropdown-multiselect', 'servicesDetails', 'ui.tinymce', 'datatables.bootstrap']);

app.config(function ($routeProvider) {
    $routeProvider
    .when('/legalMngmt', {
        templateUrl: './legalMngmt.html',
        controller: 'legalMngmtController'
    }).when('/caseType', {
        templateUrl: './caseType.html',
        controller: 'caseTypeController'
    }).when('/master', {
        templateUrl: './masterData.html',
        controller: 'masterController'
    }).when('/advocates', {
        templateUrl: './advocates.html',
        controller: 'advocatesController'
    }).when('/status', {
        templateUrl: './status.html',
        controller: 'statusController'
    }).when('/priority', {
        templateUrl: './priority.html',
        controller: 'priorityController'
    }).when('/acts', {
        templateUrl: './acts.html',
        controller: 'actsController'
    }).when('/stakeHolders', {
        templateUrl: './stakeHolders.html',
        controller: 'stakeHoldersController'
    }).when('/createCase', {
        templateUrl: './createCase.html',
        controller: 'createCaseController'
    }).when('/advocatePayment', {
        templateUrl: './advocatePayment.html',
        controller: 'advocatePaymentController'
    }).when('/hearingDate', {
        templateUrl: './hearingDate.html',
        controller: 'hearingDateController'
    }).when('/caseStakeHolders', {
        templateUrl: './caseStakeHolders.html',
        controller: 'caseStakeHoldersController'
    }).when('/noticeComments', {
        templateUrl: './noticeComments.html',
        controller: 'noticeCommentsController'
    }).when('/case', {
        templateUrl: './case.html',
        controller: 'caseController'
    }).when('/editCase', {
        templateUrl: './editCase.html',
        controller: 'editCaseController'
    }).when('/caseDetails', {
        templateUrl: './caseDetails.html',
        controller: 'caseDetailsController'
    }).when('/advocatePaymentDetails', {
        templateUrl: './advocatePaymentDetails.html',
        controller: 'advocatePaymentDetailsController'
    }).when('/noticeDetails', {
        templateUrl: './noticeDetails.html',
        controller: 'noticeDetailsController'
    }).when('/createPayments', {
        templateUrl: './createPayments.html',
        controller: 'createPaymentsController'
    }).when('/hearingDetails', {
        templateUrl: './hearingDetails.html',
        controller: 'hearingDetailsController'
    }).when('/caseUpload', {
        templateUrl: './caseUpload.html',
        controller: 'caseUploadController'
    }).when('/stakeHolders', {
        templateUrl: './stakeHolders.html',
        controller: 'stakeHoldersController'
    }).when('/writePetition', {
        templateUrl: './writePetition.html',
        controller: 'writePetitionController'
    }).when('/editWritePetition', {
        templateUrl: './editWritePetition.html',
        controller: 'editWritePetitionController'
    }).when('/court', {
        templateUrl: './court.html',
        controller: 'courtController'
    }).when('/createNotice', {
        templateUrl: './createNotice.html',
        controller: 'createNoticeController'
    }).when('/caseDepartment', {
            templateUrl: './caseDepartment.html',
            controller: 'caseDepartmentController'
        }).when('/paymentMode', {
            templateUrl: './paymentMode.html',
            controller: 'paymentModeController'
        }).when('/calendarDockets', {
            templateUrl: './calendar.html',
            controller: 'calendarController'
        }).when('/caseReports', {
            templateUrl: './caseReports.html',
            controller: 'caseReportsController'
        }).when('/createWritePetition', {
            templateUrl: './createWritePetition.html',
            controller: 'createWritePetitionController'
        }).when('/petitionDetails', {
            templateUrl: './petitionDetails.html',
            controller: 'petitionDetailsController'
        }).when('/judgement', {
            templateUrl: './judgement.html',
            controller: 'judgementController'
        }).when('/empanelGroup', {
            templateUrl: './empannelGroup.html',
            controller: 'empannelGroupController'
        }).when('/caseHistory', {
            templateUrl: './caseHistory.html',
            controller: 'caseHistoryController'
        }).when('/hearingCaseDetails', {
            templateUrl: './hearingCaseDetails.html',
            controller: 'hearingCaseDetailsController'
        }).when('/legalEmailTemplate',{
            templateUrl: './legalEmailTemplate.html',
            controller: 'legalEmailTemplateController'

        }).when('/hearingDetails/:id', {
            templateUrl: './hearingDetails.html',
            controller: 'hearingDetailsController'
        }).when('/createHearingDate', {
            templateUrl: './createHearingDate.html',
            controller: 'createHearingDateController'
        }).when('/editHearingDate', {
            templateUrl: './editHearingDate.html',
            controller: 'editHearingDateController'
        }).when('/holidays', {
            templateUrl: './holidays.html',
            controller: 'holidaysController'
        });

});


app.controller('indexController', function ($scope, $rootScope, $window, $location, $http) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
    });
    $rootScope.userLogin = false;

    $scope.reloadRoute = function() {
        $window.location.reload();
    }
    /*
     <<<<<<< HEAD
     $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
     if($scope.userInfo){
     $rootScope.userLogin = true;
     }
     $scope.logout = function(){
     =======*/
    $scope.userName = $window.localStorage.getItem('userName');

    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    if ($scope.userInfo) {
        $rootScope.userLogin = true;
    }

    var roleLogin = JSON.parse($window.localStorage.getItem('roleList'));
    $rootScope.schemeListViewTab = true;
    $rootScope.schemeRequestViewTab = true;
    $rootScope.schemeListView = true;
    $rootScope.schemeListEdit = true;
    $rootScope.schemeRequestViewTab = true;
    $rootScope.schemeRequestView = true;
    $rootScope.schemeRequestEdit = true;
    if (roleLogin) {
        $rootScope.schemeListViewTab = false;
        $rootScope.schemeRequestViewTab = false;
        $rootScope.schemeListView = false;
        $rootScope.schemeListEdit = false;
        if (roleLogin != undefined && roleLogin != null && roleLogin.length > 0) {
            for (var i = 0; i < roleLogin.length; i++) {
                if (roleLogin[i].selectFormList == 'schemeManagement') {
                    $rootScope.schemeListViewTab = true;
                    if (roleLogin[i].selectAccessLevel == 'edit') {
                        $rootScope.schemeListView = true;
                        $rootScope.schemeListEdit = true;
                    }
                    else if (roleLogin[i].selectAccessLevel == 'view') {
                        $rootScope.schemeListView = true;
                    }
                    else if (roleLogin[i].selectAccessLevel == 'all') {
                        $rootScope.schemeListView = true;
                        $rootScope.schemeListEdit = true;
                    }
                } else if (roleLogin[i].selectFormList == 'schemeRequests') {
                    $rootScope.schemeRequestViewTab = true;
                    if (roleLogin[i].selectAccessLevel == 'edit') {
                        $rootScope.schemeRequestView = true;
                        $rootScope.schemeRequestEdit = true;
                    }
                    else if (roleLogin[i].selectAccessLevel == 'view') {
                        $rootScope.schemeListView = true;
                    }
                    else if (roleLogin[i].selectAccessLevel == 'all') {
                        $rootScope.schemeRequestView = true;
                        $rootScope.schemeRequestEdit = true;
                    }
                }
            }
        }

    }

    $scope.logout = function () {
        $window.localStorage.removeItem("userDetails");
        $window.localStorage.removeItem("accessToken");
        $window.localStorage.removeItem("afterLogin");
        $window.localStorage.removeItem("userName");
        $window.location.reload();
        window.location.href = "/admin/login.html";
    };

});


//******************************CaseType Controller************************************
app.controller('caseTypeController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout, Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("caseTypeController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.caseType").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.caseType .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'caseType'}, function (response) {
//            		alert(JSON.stringify(response));
            		   if(!response){
            			window.location.href = "#/noAccessPage";
            		   }
            		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.loadingImage=true;
    $scope.getCaseTypes = function () {
        $http({
            "method": "GET",
            "url": 'api/CaseTypes',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseTypeData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getCaseTypes();
    $scope.reset = function () {
        $scope.case = angular.copy($scope.master);
        $scope.caseTYpeCreationError = false;
        $scope.createCaseTypeError = '';

    };

    $scope.caseTypeClick=function(){
        $('#CreateCaseTypeModal').modal('show');
        $scope.case = {};
        $scope.caseTYpeCreationError = false;
        $scope.createCaseTypeError = '';

        $scope.caseTYpeUpdationError = false;
        $scope.updateCaseTypeError = '';
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.case = {};
    $scope.caseTypeCheck=true;
    $scope.createCaseType = function () {

        $scope.caseTYpeCreationError = false;
        $scope.createCaseTypeError = '';
        //if ($scope.case.caseNumber.match(alphaNumeric) && $scope.case.caseNumber) {
            //if ($scope.case.caseNumber) {
            //    if ($scope.case.caseType) {

        if(  $scope.caseTypeCheck==true) {
            $scope.caseTypeCheck=false;
            console.log("posttttttttttttttt"+$scope.case.caseType);
            if ($scope.case.caseType.match(alphabetsWithSpaces) && $scope.case.caseType) {
                console.log("posttttttttttttttt")

                $http({
                    method: 'POST',
                    url: 'api/CaseTypes',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.case
                }).success(function (response) {
                    $scope.caseTypeCheck=true;
                    $scope.loadingImage=false;
                    $rootScope.caseTypeData.push(response);
                    //$scope.case = {};
                    $('#CreateCaseTypeModal').modal('hide');
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                    if (response.error.details.messages.caseType) {
                        $scope.createCaseTypeError = response.error.details.messages.caseType[0];
                        $scope.caseTYpeCreationError = true;
                        $scope.caseTypeCheck=true;
                    }

                });
            } else {
                $scope.createCaseTypeError = 'Please Enter Alphabetic Characters Only';
                $scope.caseTYpeCreationError = true;
                $scope.caseTypeCheck=true;
            }
        }

    };
    $scope.updateCaseType = {};
    $scope.editCasePopup = function (caseTypeInfo) {
        //console.log('Edit CaseTypee:' + JSON.stringify(caseTypeInfo));
        $scope.updateCaseType=angular.copy(caseTypeInfo);
//        $scope.updateCaseType = caseTypeInfo;
        console.log("update case type " + JSON.stringify($scope.updateCaseType));
    };
    $scope.caseTypeUpdateCheck=true;
    $scope.editCaseType = function () {
        $scope.caseTYpeUpdationError = false;
        $scope.updateCaseTypeError = '';

        //if ($scope.updateCaseType.caseNumber) {
        //if ($scope.updateCaseType.caseNumber.match(alphaNumeric) && $scope.updateCaseType.caseNumber) {
            //if ($scope.updateCaseType.caseType) {
        if($scope.caseTypeUpdateCheck==true) {
            $scope.caseTypeUpdateCheck = false;
            if ($scope.updateCaseType.caseType.match(alphabetsWithSpaces) && $scope.updateCaseType.caseType) {
                console.log('Edit caseType:' + JSON.stringify($scope.updateCaseType));

                $http({
                    method: 'PUT',
                    url: 'api/CaseTypes/' + $scope.updateCaseType.id,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.updateCaseType
                }).success(function (response) {
                    console.log('updateCaseType Response :' + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $scope.caseTypeUpdateCheck = true;
                    $scope.updateCaseType = {};
                    $('#caseTypeEdit').modal('hide');
                    $scope.getCaseTypes();
                }).error(function (response) {
                    console.log('CaseTypeUpdate Error');
                    console.log(JSON.stringify(response));
                    if (response.error.details.messages.caseType) {
                        $scope.updateCaseTypeError = response.error.details.messages.caseType[0];
                        $scope.caseTYpeUpdationError = true;
                        $scope.caseTypeUpdateCheck = true;
                    }
                    $timeout(function (){
                        $scope.caseTYpeUpdationError = false;
                    },3000)
                });
            } else {
                $scope.updateCaseTypeError = 'Please Enter Alphabetic Characters Only';
                $scope.caseTYpeUpdationError = true;
                $scope.caseTypeUpdateCheck = true;

                $timeout(function (){
                    $scope.caseTYpeUpdationError = false;
                },3000)

            }
            //} else {
            //    $scope.updateCaseTypeError = 'Please enter the Case Number in the Correct format';
            //    $scope.caseTYpeUpdationError = true;
            //
            //}
        }
    };
    $scope.cancelEdit = function () {
        $scope.getCaseTypes();
        $('#caseTypeEdit').modal('hide');
    };

    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.caseTypeData!=null &&  $scope.caseTypeData.length>0){

            $("<tr>" +
                "<th>Case Type</th>" +
                "<th>Case Type Description</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.caseTypeData.length;i++){
                var paymentData=$scope.caseTypeData[i];


                $("<tr>" +
                    "<td>"+paymentData.caseType+"</td>" +
                    "<td>"+paymentData.caseNumber+"</td>" +



                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'caseType');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);
//****************************CaseType Controller**************************************
//****************************Advocates Controller**************************************
app.controller('advocatesController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("advocatesController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.advocates").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.advocates .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'advocateLists'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    $scope.getAdvocates = function () {
        $http({
            "method": "GET",
            "url": 'api/Advocates',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Advocate Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.advocateData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };

    $scope.getAdvocates();
    $scope.reset = function () {
        $scope.advocate = angular.copy($scope.master);
        $scope.advocateCreationError = false;
        $scope.createAdvocateTypeError = '';

    };


    $scope.advocate = {};
    $scope.advocateClick=function(){
        $('#createAdvocateModal').modal('show');
        $scope.advocate = {};
        $scope.advocateCreationError = false;
        $scope.createAdvocateTypeError = '';
    }

    $scope.advCheck=true;
    $scope.createAdvocate = function () {
        $scope.advocateCreationError = false;
        $scope.createAdvocateTypeError = '';

        if($scope.advCheck==true) {
            $scope.advCheck=false;

            if ($scope.advocate.name.match(alphabetsWithSpaces) && $scope.advocate.name) {
                if ($scope.advocate.advId.match(alphaNumeric) && $scope.advocate.advId) {
                    if (checkMailFormat($scope.advocate.email)) {
                        if ($scope.advocate.designation.match(alphabetsWithSpaces) && $scope.advocate.designation) {
                            if ($scope.advocate.phoneNumber.match(phoneNumber) && $scope.advocate.phoneNumber.length == 10) {
                                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                                var advocate = $scope.advocate;

                                advocate['createdPerson']=loginPersonDetails.employeeId;
                                $http({
                                    method: 'POST',
                                    url: 'api/Advocates',
                                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                    "data":advocate
                                }).success(function (response) {
                                    $scope.loadingImage=false;
                                    $scope.advCheck = true;
                                    $rootScope.advocateData.push(response);
                                    //alert(JSON.stringify($rootScope.advocateData));


                                    $('#createAdvocateModal').modal('hide');
                                }).error(function (response) {
                                    console.log('Post Advocate Error Response :' + JSON.stringify(response));
                                    if (response.error.details.messages.email) {
                                        $scope.createAdvocateTypeError = response.error.details.messages.email[0];
                                        $scope.advocateCreationError = true;
                                        $scope.advCheck=true;
                                        $timeout(function (){
                                            $scope.advocateCreationError = false;
                                        },3000)
                                    }
                                    else if(response.error.details.messages.advId){
                                        $scope.createAdvocateTypeError = response.error.details.messages.advId[0];
                                        $scope.advocateCreationError = true;
                                        $scope.advCheck=true;
                                        $timeout(function (){
                                            $scope.advocateCreationError = false;
                                        },3000)
                                    }

                                });
                            } else {
                                $scope.createAdvocateTypeError = 'Please Enter the Phone Number in the Correct Format';
                                $scope.advocateCreationError = true;
                                $scope.advCheck=true;
                                $timeout(function (){
                                    $scope.advocateCreationError = false;
                                },3000)

                            }
                        } else {
                            $scope.createAdvocateTypeError = 'Please Enter Designation in the Alphabetic Characters Only';
                            $scope.advocateCreationError = true;
                            $scope.advCheck=true;
                            $timeout(function (){
                                $scope.advocateCreationError = false;
                            },3000)

                        }
                    } else {
                        $scope.createAdvocateTypeError = 'Please enter the Email in the Correct format';
                        $scope.advocateCreationError = true;
                        $scope.advCheck=true;
                        $timeout(function (){
                            $scope.advocateCreationError = false;
                        },3000)

                    }
                }else{
                    $scope.createAdvocateTypeError = 'Please enter the Advocate Id in  Alpha Numaric Only';
                    $scope.advocateCreationError = true;
                    $scope.advCheck=true;
                    $timeout(function (){
                        $scope.advocateCreationError = false;
                    },3000)
                }

            } else {
                $scope.createAdvocateTypeError = 'Please enter the Name in  Alphabetic Characters Only';
                $scope.advocateCreationError = true;
                $scope.advCheck=true;
                $timeout(function (){
                    $scope.advocateCreationError = false;
                },3000)

            }
        }
    };
    $scope.updateAdvocate = {};
    $scope.editAdvocatePopup = function (advocateInfo) {
        console.log('Edit Advocate:' + JSON.stringify(advocateInfo));
        $scope.updateAdvocate = advocateInfo;
    };

    $scope.editAdvocate = function () {
        $scope.advocateUpdationError = false;
        $scope.updateAdvocateTypeError = '';
        console.log('Edit Advocate1:' + JSON.stringify($scope.updateAdvocate));
        if ($scope.updateAdvocate.name.match(alphabetsWithSpaces) && $scope.updateAdvocate.name) {
            if ($scope.updateAdvocate.advId.match(alphaNumeric) && $scope.updateAdvocate.advId) {
                if (checkMailFormat($scope.updateAdvocate.email)) {
                    if ($scope.updateAdvocate.designation.match(alphabetsWithSpaces) && $scope.updateAdvocate.designation) {
                        if ($scope.updateAdvocate.phoneNumber.match(phoneNumber) && $scope.updateAdvocate.phoneNumber.length == 10) {
                            $http({
                                method: 'PUT',
                                url: 'api/Advocates/' + $scope.updateAdvocate.id,
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                data: $scope.updateAdvocate
                            }).success(function (response) {
                                $scope.loadingImage=false;
                                //console.log('updateAdvocate Response :' + JSON.stringify(response));
                                $scope.updateAdvocate = {};
                                $scope.getAdvocates();
                                $('#advocateEdit').modal('hide');
                            }).error(function (response) {
                                console.log('updateAdvocate Error'+JSON.stringify(response));
                                if (response.error.details.messages.email) {
                                    $scope.updateAdvocateTypeError = response.error.details.messages.email[0];
                                    $scope.advocateUpdationError = true;
                                }

                                else if (response.error.details.messages.advId) {
                                    $scope.updateAdvocateTypeError = response.error.details.messages.advId[0];
                                    $scope.advocateUpdationError = true;
                                }
                                $timeout(function (){
                                    $scope.advocateUpdationError = false;
                                },3000)
                            });

                        } else {
                            $scope.updateAdvocateTypeError = 'Please Enter the Phone Number in the Correct Format';
                            $scope.advocateUpdationError = true;

                            $timeout(function (){
                                $scope.advocateUpdationError = false;
                            },3000)

                        }
                    } else {
                        $scope.updateAdvocateTypeError = 'Please Enter the Designation in Alphabetic Characters Only';
                        $scope.advocateUpdationError = true;

                        $timeout(function (){
                            $scope.advocateUpdationError = false;
                        },3000)

                    }
                } else {
                    $scope.updateAdvocateTypeError = 'Please enter the Email in the Correct format';
                    $scope.advocateUpdationError = true;

                    $timeout(function (){
                        $scope.advocateUpdationError = false;
                    },3000)

                }
            }else{
                $scope.updateAdvocateTypeError = 'Please enter the Advocate Id in Alpha Numaric Only';
                $scope.advocateUpdationError = true;

                $timeout(function (){
                    $scope.advocateUpdationError = false;
                },3000)
            }
        } else {
            $scope.updateAdvocateTypeError = 'Please enter the Name in Alphabetic Characters Only';
            $scope.advocateUpdationError = true;

            $timeout(function (){
                $scope.advocateUpdationError = false;
            },3000)

        }

    };
    $scope.cancelEdit = function () {
        $scope.getAdvocates();
        $('#advocateEdit').modal('hide');
    };


    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.advocateData!=null &&  $scope.advocateData.length>0){

            $("<tr>" +
                "<th>Name</th>" +
                "<th>Advocate Id</th>" +
                "<th>Email</th>" +
                "<th>Designation</th>" +
                "<th>PhoneNumber</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.advocateData.length;i++){
                var paymentData=$scope.advocateData[i];


                $("<tr>" +
                    "<td>"+paymentData.name+"</td>" +
                    "<td>"+paymentData.advId+"</td>" +
                    "<td>"+paymentData.email+"</td>" +
                    "<td>"+paymentData.designation+"</td>" +
                    "<td>"+paymentData.phoneNumber+"</td>" +



                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'advocates');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
//****************************Advocates Controller**************************************
//****************************Status Controller**************************************
app.controller('statusController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("statusController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.status").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.status .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'status'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.getStatus = function () {
        $http({
            "method": "GET",
            "url": 'api/Statuses',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Status Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.statusData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getStatus();
    $scope.reset = function () {
        $scope.status = angular.copy($scope.master);
        $scope.statusCreationError = false;
    };

    $scope.statusClick=function(){
        $('#createStatusModal').modal('show');
        $scope.status = {};
        $scope.statusCreationError = false;
        $scope.createStatusTypeError = '';
    }
    $scope.status = {};
    $scope.statusCheck=true;
    $scope.createStatus = function () {
        $scope.statusCreationError = false;
        $scope.createStatusTypeError = '';

        if ($scope.statusCheck == true) {
            $scope.statusCheck=false;
        if($scope.status.field1.match(alphabetsWithSpaces) && $scope.status.field1) {

                $http({
                    method: 'POST',
                    url: 'api/Statuses',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.status
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.statusCheck=true;
                    $rootScope.statusData.push(response);
                    //$scope.status = {};
                    $('#createStatusModal').modal('hide');
                }).error(function (response) {
                    console.log('Post Status Error Response :' + JSON.stringify(response));
                    if (response.error.details.messages.field1) {
                        $scope.createStatusTypeError = response.error.details.messages.field1[0];
                        $scope.statusCreationError = true;
                        $scope.statusCheck=true;
                    }

                });
            } else {
                $scope.createStatusTypeError = 'Please Enter the Status in the Correct Format';
                $scope.statusCreationError = true;
            $scope.statusCheck=true;
            }
            }
        //}}
    };
    $scope.updateStatus = {};
    $scope.editStatusPopup = function (statusInfo) {
        console.log('Edit Status:' + JSON.stringify(statusInfo));
        $scope.updateStatus = statusInfo;
    };

    $scope.editStatus = function () {
        $scope.statusUpdationError = false;
        $scope.updateStatusTypeError = '';

        if($scope.updateStatus.field1.match(alphabetsWithSpaces) && $scope.updateStatus.field1){
        console.log('Edit Status1:' + JSON.stringify($scope.updateStatus));

        $http({
            method: 'PUT',
            url: 'api/Statuses/' + $scope.updateStatus.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateStatus
        }).success(function (response) {
            //console.log('updateStatus Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.updateStatus = {};
            $scope.getStatus();
            $('#statusEdit').modal('hide');
        }).error(function (response) {
            console.log('StatusUpdate Error');
            if (response.error.details.messages.field1) {
                $scope.updateStatusTypeError = response.error.details.messages.field1[0];
                $scope.statusUpdationError = true;
            }
            $timeout(function (){
                $scope.statusUpdationError = false;
            },3000)

        });
        }else {
            $scope.updateStatusTypeError = 'Please Enter the Status in the Correct Format';
            $scope.statusUpdationError = true;
            $timeout(function (){
                $scope.statusUpdationError = false;
            },3000)

        }
    };
    $scope.cancelEdit = function () {
        $scope.getStatus();
        $('#statusEdit').modal('hide');
    };


    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.statusData!=null &&  $scope.statusData.length>0){

            $("<tr>" +
                "<th>Status</th>" +
                "<th>Description</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.statusData.length;i++){
                var paymentData=$scope.statusData[i];


                $("<tr>" +
                    "<td>"+paymentData.field1+"</td>" +
                    "<td>"+paymentData.field2+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'status');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }
}]);
//****************************Status Controller**************************************
//****************************priorityController **************************************
app.controller('priorityController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("priorityController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.priority").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.priority .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'priority'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;

    $scope.getPriority = function () {
        $http({
            "method": "GET",
            "url": 'api/Priorities',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Priority Get Response :' + JSON.stringify(response));
            $rootScope.priorityData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getPriority();
    $scope.reset = function () {
        $scope.priority = angular.copy($scope.master);
        $scope.priorityCreationError = false;
        $scope.priorityDescriptionError = '';
    };

    $scope.priorityClick=function(){
        $('#createPriorityModal').modal('show');
        $scope.priority = {};
        $scope.priorityCreationError = false;
        $scope.priorityDescriptionError = '';
    }
    $scope.priority = {};
    $scope.priorityCheck=true;
    $scope.createPriority = function () {

        $scope.priorityCreationError = false;
        $scope.priorityDescriptionError = '';


     if($scope.priorityCheck==true) {
    $scope.priorityCheck=false;
    if ($scope.priority.priority.match(alphabetsWithSpaces) && $scope.priority.priority) {

        $http({
            method: 'POST',
            url: 'api/Priorities',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.priority
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.priorityCheck=true;
            $rootScope.priorityData.push(response);
            //$scope.priority = {};
            $('#createPriorityModal').modal('hide');
        }).error(function (response) {
            console.log('Post Priority Error Response :' + JSON.stringify(response));
            if (response.error.details.messages.priority) {
                $scope.priorityDescriptionError = response.error.details.messages.priority[0];
                $scope.priorityCreationError = true;
                $scope.priorityCheck=true;
            }

        });
    } else {
        $scope.priorityDescriptionError = 'Please Provide The Priority in the Correct Format';
        $scope.priorityCreationError = true;
        $scope.priorityCheck=true;

    }
}
    };


    $scope.updatePriority = {};
    $scope.editPriorityPopup = function (priorityInfo) {
        console.log('Edit Priority:' + JSON.stringify(priorityInfo));
        $scope.updatePriority = priorityInfo;
    };

    $scope.editPriority = function () {

        $scope.priorityUpdationError = false;
        $scope.updatePriorityTypeError = '';

        console.log('Edit priority:' + JSON.stringify($scope.updatePriority));
        if($scope.updatePriority) {
            delete  $scope.updatePriority["$$hashKey"]
        }
        if ($scope.updatePriority.priority.match(alphabetsWithSpaces) && $scope.updatePriority.priority) {

            $http({
                method: 'PUT',
                url: 'api/Priorities/' + $scope.updatePriority.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updatePriority
            }).success(function (response) {
                //console.log('Priority Response :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $scope.updatePriority = {};
                $scope.getPriority();
                $('#priorityEdit').modal('hide');
            }).error(function (response) {
                console.log('PriorityUpdate Error'+JSON.stringify(response));

                if (response.error.details.messages.priority) {
                    $scope.updatePriorityTypeError = response.error.details.messages.priority[0];
                    $scope.priorityUpdationError = true;
                }
                $timeout(function (){
                    $scope.priorityUpdationError = false;
                },3000)
            });


        } else {
            $scope.updatePriorityTypeError = 'Please Update the Priority in the Correct Format';
            $scope.priorityUpdationError = true;
            $timeout(function (){
                $scope.priorityUpdationError = false;
            },3000)
        }

    };


    $scope.cancelEdit = function () {
        $scope.getPriority();
        $('#priorityEdit').modal('hide');
    };


    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.priorityData!=null &&  $scope.priorityData.length>0){

            $("<tr>" +
                "<th>Priority</th>" +
                "<th>Description</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.priorityData.length;i++){
                var paymentData=$scope.priorityData[i];


                $("<tr>" +
                    "<td>"+paymentData.priority+"</td>" +
                    "<td>"+paymentData.description+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'priority');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);

//****************************priorityController **************************************
//****************************ActsController ******************************************
app.controller('actsController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("actsController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.acts").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.acts .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'acts'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    var alphaNumeric =/^[a-z\d\-_\s]+$/i;
    $scope.getAct = function () {
        $http({
            "method": "GET",
            "url": 'api/Acts',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Act Get Response :' + JSON.stringify(response));
            $scope.loadingImage= false;
            $rootScope.actData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getAct();
    $scope.reset = function () {
        $scope.act = angular.copy($scope.master);
        $scope.actCreationError = false;
        $scope.createActError = '';
    };

   $scope.actClick=function(){
       $('#createActModal').modal('show');
       $scope.act = {};
       $scope.actCreationError = false;
       $scope.createActError = '';
   }
    $scope.act = {};
    $scope.actCheck=true;
    $scope.createAct = function () {
        $scope.actCreationError = false;
        $scope.createActError = '';

        if($scope.actCheck==true) {
            $scope.actCheck = false;
            if ($scope.act.actNumber.match(alphaNumeric) && $scope.act.actNumber) {
                $http({
                    method: 'POST',
                    url: 'api/Acts',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.act
                }).success(function (response) {
                    $scope.loadingImage= false;
                    $scope.actCheck = true;
                    $rootScope.actData.push(response);
                    //$scope.act = {};
                    $('#createActModal').modal('hide');
                }).error(function (response) {
                    console.log('Post Act Error Response :' + JSON.stringify(response));
                    if (response.error.details.messages.actNumber) {
                        $scope.createActError = response.error.details.messages.actNumber[0];
                        $scope.actCreationError = true;
                        $scope.actCheck = true;
                    }
                });
            } else {
                $scope.createActError = 'Please Enter the Act Number in Alpha Numaric Format';

                $scope.actCreationError = true;
                $scope.actCheck = true;

            }
        }
    };
    $scope.updateAct = {};
    $scope.editActPopup = function (actInfo) {
        console.log('Edit updateAct:' + JSON.stringify(actInfo));
        $scope.updateAct = actInfo;
    };
    $scope.editAct = function () {

        $scope.actUpdationError = false;
        $scope.updateActError = '';


        if ($scope.updateAct.actNumber.match(alphaNumeric) && $scope.updateAct.actNumber) {
            console.log('Edit Act1:' + JSON.stringify($scope.updateAct));

            $http({
                method: 'PUT',
                url: 'api/Acts/' + $scope.updateAct.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updateAct
            }).success(function (response) {
                //console.log('Act Response :' + JSON.stringify(response));
                $scope.loadingImage= false;
                $scope.updateAct = {};
                $scope.getAct();
                $('#actEdit').modal('hide');
            }).error(function (response) {
                console.log('ActUpdate Error');
                if (response.error.details.messages.actNumber) {
                    $scope.updateActError = response.error.details.messages.actNumber[0];
                    $scope.actUpdationError = true;
                }
                $timeout(function (){
                    $scope.actUpdationError = false;
                    },3000)
            });
        }else{
            $scope.updateActError = 'Please Enter The ActNumber in the Correct Format';
            $scope.actUpdationError = true;
            $timeout(function (){
                $scope.actUpdationError = false;
            },3000)

        }

    };
    $scope.cancelEdit = function () {
        $scope.getAct();
        $('#actEdit').modal('hide');
    };

    $scope.reset();
    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.actData!=null &&  $scope.actData.length>0){

            $("<tr>" +
                "<th>Act Number</th>" +
                "<th>Description</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.actData.length;i++){
                var paymentData=$scope.actData[i];


                $("<tr>" +
                    "<td>"+paymentData.actNumber+"</td>" +
                    "<td>"+paymentData.viewDetails+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'acts');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
//****************************ActsController **************************************
//****************************StakeHolderController **************************************
app.controller('stakeHoldersController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("stakeHoldersController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.stakeHolders").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.stakeHolders .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'stakeHolders'}, function (response) {
//					alert(JSON.stringify(response));
            		   if(!response){
            			window.location.href = "#/noAccessPage";
            		   }
            		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;

    $scope.getStakeHolders = function () {
        $http({
            "method": "GET",
            "url": 'api/StakeHolders',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getStakeHoldersss  Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.stakeHolderData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getStakeHolders();
    $scope.reset = function () {
        $scope.stakeHolder = angular.copy($scope.master);
        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';
    };

    $scope.stakeHolderClick=function(){
        $('#craeteStakeHolderModal').modal('show');
        $scope.stakeHolder = {};
        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';
    }

    $scope.stakeHolder = {};
    $scope.holderCheck=true;

    $scope.createMasterStakeHolders = function () {

        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';

        if($scope.holderCheck==true) {
            $scope.holderCheck = false;
            if ($scope.stakeHolder.name.match(alphabetsWithSpaces) && $scope.stakeHolder.name) {
                if ($scope.stakeHolder.holderId.match(alphaNumeric) && $scope.stakeHolder.holderId) {
                if ($scope.stakeHolder.department.match(alphabetsWithSpaces) && $scope.stakeHolder.department) {
                    if ($scope.stakeHolder.designation.match(alphabetsWithSpaces) && $scope.stakeHolder.designation) {
                        if (checkMailFormat($scope.stakeHolder.email)) {
                            if ($scope.stakeHolder.phone.match(phoneNumber) && $scope.stakeHolder.phone.length == 10) {

                                $http({
                                    method: 'POST',
                                    url: 'api/StakeHolders',
                                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                    data: $scope.stakeHolder
                                }).success(function (response) {
                                    $scope.loadingImage=false;
                                    $scope.holderCheck = true;
                                    $rootScope.stakeHolderData.push(response);
                                    //$scope.stakeHolder = {};
                                    $('#craeteStakeHolderModal').modal('hide');
                                }).error(function (response) {
                                    console.log('Post stakeHolder Error Response :' + JSON.stringify(response));
                                    if (response.error.details.messages.email) {
                                        $scope.createHolderTypeError = response.error.details.messages.email[0];
                                        $scope.holderCreationError = true;
                                        $scope.holderCheck = true;
                                    }
                                   else if (response.error.details.messages.holderId) {
                                        $scope.createHolderTypeError = response.error.details.messages.holderId[0];
                                        $scope.holderCreationError = true;
                                        $scope.holderCheck = true;
                                    }
                                });
                            } else {
                                $scope.createHolderTypeError = 'Please Enter The Phone Number In the Required Format';
                                $scope.holderCreationError = true;
                                $scope.holderCheck = true;
                            }
                        } else {
                            $scope.createHolderTypeError = 'Please Enter The Email In the Required Format';
                            $scope.holderCreationError = true;
                            $scope.holderCheck = true;
                        }
                    } else {
                        $scope.createHolderTypeError = 'Please Enter The Designation In the Alphabet';
                        $scope.holderCreationError = true;
                        $scope.holderCheck = true;
                    }
                } else {
                    $scope.createHolderTypeError = 'Please Enter The Department In the Alphabet';
                    $scope.holderCreationError = true;
                    $scope.holderCheck = true;
                }
                } else {
                    $scope.createHolderTypeError = 'Please Enter The Stake Holder Id In the Correct Format';
                    $scope.holderCreationError = true;
                    $scope.holderCheck = true;
                }
            } else {
                $scope.createHolderTypeError = 'Please Enter The StakeHolder Name In the Alphabet';
                $scope.holderCreationError = true;
                $scope.holderCheck = true;

            }
        }
    };
    $scope.updateStakeHolder = {};
    $scope.editStakeHolderPopup = function (stakeHolderInfo) {
        console.log('Edit updateStakeHolder:' + JSON.stringify(stakeHolderInfo));
        $scope.updateStakeHolder = stakeHolderInfo;
    };

    $scope.editStakeHolder = function () {
        console.log('Edit stakeHolder1:' + JSON.stringify($scope.updateStakeHolder));

        $scope.holderUpdationError = false;
        $scope.updateHolderTypeError = '';

        if($scope.updateStakeHolder.name.match(alphabetsWithSpaces) && $scope.updateStakeHolder.name){
            if($scope.updateStakeHolder.department.match(alphabetsWithSpaces) && $scope.updateStakeHolder.department){
                if($scope.updateStakeHolder.designation.match(alphabetsWithSpaces) && $scope.updateStakeHolder.designation){
                    if(checkMailFormat($scope.updateStakeHolder.email)) {
                        if ($scope.updateStakeHolder.phone.match(phoneNumber) && $scope.updateStakeHolder.phone.length == 10) {

        $http({
            method: 'PUT',
            url: 'api/StakeHolders/' + $scope.updateStakeHolder.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateStakeHolder
        }).success(function (response) {
            //console.log('updateStakeHolder Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.updateStakeHolder = {};
            $scope.getStakeHolders();
            $('#stakeHolderEdit').modal('hide');
        }).error(function (response) {
            console.log('stakeHolderUpdate Error');
            if (response.error.details.messages.email) {
                $scope.updateHolderTypeError = response.error.details.messages.email[0];
                $scope.holderUpdationError = true;
            }

            $timeout(function (){
                $scope.holderUpdationError = false;
            },3000)
        });
                        }else{
                            $scope.updateHolderTypeError = 'Please Enter The Phone Number In the Required Format';
                            $scope.holderUpdationError = true;
                            $timeout(function (){
                                $scope.holderUpdationError = false;
                            },3000)
                        }
                    }else{
                        $scope.updateHolderTypeError = 'Please Enter The Email In the Required Format';
                        $scope.holderUpdationError = true;
                        $timeout(function (){
                            $scope.holderUpdationError = false;
                        },3000)
                    }
                }else{
                    $scope.updateHolderTypeError = 'Please Enter The Designation In the Required Format';
                    $scope.holderUpdationError = true;
                    $timeout(function (){
                        $scope.holderUpdationError = false;
                    },3000)
                }
            }else{
                $scope.updateHolderTypeError = 'Please Enter The Department In the Required Format';
                $scope.holderUpdationError = true;

                $timeout(function (){
                    $scope.holderUpdationError = false;
                },3000)
            }
        }else{
            $scope.updateHolderTypeError = 'Please Enter The StakeHolder Name In the Required Format';
            $scope.holderUpdationError = true;
            $timeout(function (){
                $scope.holderUpdationError = false;
            },3000)

        }

    };
    $scope.cancelEdit = function () {
        $scope.getStakeHolders();
        $('#stakeHolderEdit').modal('hide');
    };


    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.stakeHolderData!=null &&  $scope.stakeHolderData.length>0){

            $("<tr>" +
                "<th>StakeHolder Name</th>" +
                "<th>Department</th>" +
                "<th>Designation</th>" +
                "<th>Email</th>" +
                "<th>Phone</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.stakeHolderData.length;i++){
                var paymentData=$scope.stakeHolderData[i];


                $("<tr>" +
                    "<td>"+paymentData.name+"</td>" +
                    "<td>"+paymentData.department+"</td>" +
                    "<td>"+paymentData.designation+"</td>" +
                    "<td>"+paymentData.email+"</td>" +
                    "<td>"+paymentData.phone+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'stakeHolders');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
//****************************StakeHolderController **************************************
//****************************CreateCaseController **************************************
app.controller('createCaseController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
    console.log("createCaseController Entered...");

    $scope.uploadFile = uploadFileURL;

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {
//        alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;

    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;

    $scope.docUploadURL = 'apiUploads/dhanbadDb/download/';
    $scope.dtOptions = {paging: false, searching: false};

    $scope.getCaseTypes = function () {
        $http({
            method: 'GET',
            url: 'api/CaseTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('GetCaseType Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseTypes = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseTypes();


    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getStatus Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.status = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getStatus();

    $scope.getCourt = function () {
        $http({
            method: 'GET',
            url: 'api/Courts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('courtName Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.courtName = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCourt();

    $scope.getPriority = function () {
        $http({
            method: 'GET',
            url: 'api/Priorities',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getPriority Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.priority = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getPriority();

    $scope.getActs = function () {
        $http({
            method: 'GET',
            url: 'api/Acts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getActs Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.acts = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getActs();

    $scope.getDepartment = function () {
        $http({
            method: 'GET',
            url: 'api/caseDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('caseDepartments Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.department = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getDepartment();


    $scope.getStakeHolders = function () {
        $http({
            method: 'GET',
            url: 'api/StakeHolders',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getStakeHolders Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.holders = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getStakeHolders();

    $scope.getEmapnnel = function () {
        $http({
            method: 'GET',
            url: 'api/EmpannelGroups',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getEmapnnel Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.empannels = response;
        }).error(function (response) {
            console.log('Error getEmapnnel Response :' + JSON.stringify(response));
        });
    };
    $scope.getEmapnnel();

    $scope.getAdvocates = function (emp) {
        //alert(emp);
        $http({
            method: 'GET',
            url: 'api/EmpannelGroups?filter={"where":{"groupName":"'+emp+'"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           // console.log('getadvocates Response :' + JSON.stringify(response));
            $scope.advocates = response[0].advocateList;
            //alert( JSON.stringify($scope.advocates));

        }).error(function (response) {
            console.log('Error getEmapnnel Response :' + JSON.stringify(response));
        });
    };
    //$scope.getAdvocates();


    //******get case method**************

    $scope.getCase = function () {
        $http({
            "method": "GET",
            "url": 'api/CreateCases',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('create case Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getCase();


    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;

        $scope.advocates={};
        $scope.CaseCreationError = false;
        $scope.CaseError = '';
    };

    //$scope.addCaseModal = function() {
    //    $scope.user = {};
    //    $("#addDesignation").modal("show");
    //}
    $scope.user = {};
    //console.log("create case function entered");
    $scope.createCaseCheck=true;
    $scope.createCase = function () {
        $scope.CaseCreationError = false;
        $scope.CaseError = '';

        //alert('entered');
        if ($scope.createCaseCheck == true) {
            $scope.createCaseCheck = false;
            if (formUploadStatus) {
                //console.log("@@@@@@@@@@@@@@@@@@users response@@@@@@@@@@@@@@" + JSON.stringify($scope.user));
                $scope.loadingImage=false;

                $scope.advocateData = [];
                for(var i=0;i<$scope.user.advocate.length;i++){
                    $scope.advocateData.push(JSON.parse($scope.user.advocate[i]));
                }
                $scope.user.advocate = [];

                for(var i=0;i<$scope.advocateData.length;i++){
                    //alert("in for");
                    $scope.user.advocate.push({
                        name:$scope.advocateData[i].name,
                        email:$scope.advocateData[i].email,
                        advId:$scope.advocateData[i].advId,
                        phoneNumber:$scope.advocateData[i].phoneNumber
                    });
                    //alert(JSON.stringify($scope.hearing.advocateName));
                }
                //console.log(JSON.stringify($scope.user.advocateName));
                if ($scope.user.caseDate) {
                    var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                    $scope.user['createdPerson'] = loginPersonDetails.employeeId;
                        alert(JSON.stringify($scope.user))
                    $http({
                        method: 'POST',
                        url: 'api/CreateCases',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        data: $scope.user
                    }).success(function (response) {
                        $scope.loadingImage=false;
                        $scope.createCaseCheck=true;
                      // alert('CreateCase Response: :' + JSON.stringify(response));
                        $rootScope.caseData.push(response);

                       //alert("$rootScope.caseData*************" + JSON.stringify($rootScope.caseData));
                        $scope.selectCaseNumber = response.caseNumber;
                        $window.localStorage.setItem('caseCreationCheck', $scope.selectCaseNumber);
                       // alert("$rootScope.selectCaseNumber*************" + JSON.stringify($scope.selectCaseNumber));
                        $scope.caseUploads();
//alert("After Success and before location href");
                        location.href = '#/case';

//                        $window.location.reload();
                        ///*$scope.user = {};*/
                        //$('#myModal').modal('hide');

                        //$("#addDesignation").modal("hide");
                        //
                        //$('#addDepartmentSuccess').modal('show');
                        //setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
                    }).error(function (response) {
                        console.log('Error Response1 :' + JSON.stringify(response));
                        if(response.error.message){
                        $scope.CaseCreationError = true;
                        $scope.CaseError =response.error.message;
                         $scope.createCaseCheck=true;
                        }

                        $scope.createCaseCheck=true;
                    });
                } else {
                    $scope.CaseCreationError = true;
                    $scope.CaseError = 'Please Select Case Submission Date';
                    $scope.createCaseCheck=true;
                }
            } else {
                $scope.CaseCreationError = true;
                $scope.CaseError = 'Please Upload the files';
                $scope.createCaseCheck=true;
            }
        }
    };
    //$scope.addDepartmentSuccessModal = function() {
    //    //alert("addDepartmentSuccessModal");
    //    $('#addDepartmentSuccess').modal('hide');
    //    $window.location.reload();
    //    location.href = '#/case';
    //}

    //*********************************File Upload for Create Case************************************
    $scope.docUploadURL = 'apiUploads/dhanbadDb/download/';
    $scope.dtOptions = {paging: false, searching: false};
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('CaseNumber  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getCaseNumber();


//******file Upload***********
    var formIdsArray = [];
    var formUploadStatus = false;
    $scope.disable = true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        //var count=1;
        //  filedetails=[];
        formIdsArray = [];
        //$scope.docList = [];
        //$scope.forms = files;
        var fileCount = 0;
        angular.forEach(files, function (file) {
            $scope.disable = true;
            $scope.errorMssg1 = true;
            $scope.fileUploadSuccess = false;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}

                });
// alert(JSON.stringify(data))
                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if(fileCount == files.length){
                $scope.disable = false;
                $scope.errorMssg1 = false;
                $scope.fileUploadSuccess = true;
            }
        });
    };
//******file Upload***********

    $scope.caseUploads = function () {
         //alert('hai');
        console.log("case uploads entered");
        if (formUploadStatus) {
            var uploadDetails = $scope.upload;
            uploadDetails.file = $scope.docList;
            var caseNumber = $scope.selectCaseNumber;
            //console.log("&&&&&&&&&&&&&&&&&&&&7"+caseNumber);
            uploadDetails['caseNumber'] = caseNumber;
            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            uploadDetails['createdPerson'] = loginPersonDetails.name;
            uploadDetails['uploadedPersonName'] = loginPersonDetails.name;
              //alert('uplaoded data is'+JSON.stringify($scope.docList));
            uploadDetails.status = 'active';
            $http({
                method: 'POST',
                url: 'api/CaseUploads',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data": uploadDetails
            }).success(function (response) {
                //console.log('docs  Response :' + JSON.stringify(response));
                //console.log("post case uploads");
                $scope.loadingImage=false;
                $("#scroll2").hide();
                $("#scroll").show();
                $scope.docList = [];
                $scope.upload = {};
                $scope.getDocuments();

            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
            //console.log('scheme details are'+JSON.stringify(schemeDetails));
        } else {
            //$scope.createScheme();
        }
    }


    $scope.getDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/CaseUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.documentsList = response;
            //console.log("caseupload docs upload response");
            $scope.loadingImage=false;
               //alert('hai'+response.length);
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }

//***********select*************

    $scope.deleteFile = function(fileId, index){
     $scope.fileUploadSuccess = false;
        //alert("RRRR"+fileId, index);
        console.log('DocList Before:'+JSON.stringify($scope.docList));
        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
            //console.log('DocList After:'+JSON.stringify($scope.docList));
        });
    }



    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
    });

    //******************* File Upload For Create Case *****************************************
$scope.reset();
}]);

//****************************CreateCaseController **************************************
//****************************advocatePaymentController **************************************

app.controller('advocatePaymentController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope','$timeout', 'Excel','DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window,Excel,$timeout, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {
    console.log("advocatePaymentController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

     var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'advocatePayments'}, function (response) {
//        alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    $scope.getPayments = function () {
        $http({
            "method": "GET",
            "url": 'api/AdvocatePayments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('advocatePaymentController Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.paymentData = response;
            $scope.paymentGetData = response;
 console.log('advocatePaymentController Get Response--- :' + JSON.stringify($rootScope.paymentData));
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getPayments();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    //$scope.user = {};
    if($window.localStorage.getItem('paymentCreationCheck')){
        $scope.caseShow=true;
        $scope.caseHide=false;

       $scope.advName= $window.localStorage.getItem('CreationCheck');
        $scope.selectedCaseNumber = $window.localStorage.getItem('paymentCreationCheck')
        $window.localStorage.removeItem('paymentCreationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }


    $scope.payment = {};

    $scope.updatePayment = {};
    $scope.editPaymentPopup = function (paymentInfo) {
       // console.log('Edit paymentInfo:' + JSON.stringify(paymentInfo));
        $scope.updatePayment = paymentInfo;
    };

    $scope.editPayment = function () {

        $scope.advocateData = [];
        console.log('Edit Payment:::::::::' + JSON.stringify($scope.updatePayment.advocateName));
        $scope.advocateData.push(JSON.parse($scope.updatePayment.advocateName));
         console.log('Edit Payment100000000000:' + JSON.stringify($scope.advocateData));

        $scope.updatePayment.advocateName=[];
         for(var i=0;i<$scope.advocateData.length;i++){


             $scope.updatePayment.advocateName.push($scope.advocateData[i].advId);

         }
        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
        $scope.updatePayment['lastEditPerson'] = loginPersonDetails.employeeId;

        $http({
            method: 'PUT',
            url: 'api/AdvocatePayments/' + $scope.updatePayment.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updatePayment
        }).success(function (response) {
            console.log('payment Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.advocatName=response.advocateName;
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            $window.location.reload();
        }).error(function (response) {
            console.log('paymentUpdate Error');
        });

    };
    $scope.cancelEdit = function () {
        $scope.getPayments();
        $('#paymentEdit').modal('hide');
    };

    $scope.reset();

    //*************get CaseNumber*******
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getAdvocates Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.advocates = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getAdvocates();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.paymentData!=null &&  $scope.paymentData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Advocate Name</th>" +
                "<th>Amount Paid</th>" +
                "<th>Mode of Payment</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.paymentData.length;i++){
                var paymentData=$scope.paymentData[i];


                $("<tr>" +
                    "<td>"+paymentData.caseNumber+"</td>" +
                    "<td>"+paymentData.advocateName+"</td>" +
                    "<td>"+paymentData.amount+"</td>" +
                    "<td>"+paymentData.mode+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'advocatePayment ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }


}]);
//****************************advocatePaymentController **************************************
//****************************hearingDateController **************************************
app.controller('hearingDateController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Upload', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("hearingDateController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'hearingDateInfo'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.reloadRoute = function() {
        $window.location.reload();
    }
    $scope.gethearingDate = function () {
        $http({
            "method": "GET",
            "url": 'api/HearingDates',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            // console.log('*****hearingDateController Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.hearingData = response;
            console.log(JSON.stringify( $rootScope.hearingData))
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.gethearingDate();

    //$scope.reset = function () {
    //    $scope.hearing = angular.copy($scope.master);
    //};
    $scope.hearing = {};

    //$scope.getEditHearing=function (hearingInfo) {
    //
    //    console.log("getEditCase:::" +JSON.stringify(hearingInfo));
    //    $rootScope.updateHearingDate=hearingInfo;
    //}


    if($window.localStorage.getItem('hearingCreationCheck')){
        $scope.caseShow=true;
        $scope.caseHide=false;

        $scope.selectedCaseNumber = $window.localStorage.getItem('hearingCreationCheck')
        $window.localStorage.removeItem('hearingCreationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }

    if($window.localStorage.getItem('hearingUpdationCheck')){
        $scope.caseShow=false;
        $scope.caseHide=true;
        $scope.selectedCaseNumb = $window.localStorage.getItem('hearingUpdationCheck')
        $window.localStorage.removeItem('hearingUpdationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }

    $scope.reset = function () {
        $scope.hearing = angular.copy($scope.master);
    };
    //$scope.hearingDateClick=function(){
    //    $('#createHearingDateModal').modal('show');
    //    $scope.hearing = {};
    //
    //}
    $scope.hearing = {};
    $scope.hearingCheck=true;
    //$scope.createHearingDate = function () {
    //    //alert(JSON.stringify($scope.hearing.advocateName));
    //
    //    $scope.advocateData = [];
    //
    //    for(var i=0;i<$scope.hearing.advocateName.length;i++){
    //        $scope.advocateData.push(JSON.parse($scope.hearing.advocateName[i]));
    //    }
    //    $scope.hearing.advocateName = [];
    //
    //    for(var i=0;i<$scope.advocateData.length;i++){
    //        //alert("in for");
    //        $scope.hearing.advocateName.push({
    //            name:$scope.advocateData[i].name,
    //            email:$scope.advocateData[i].email
    //        });
    //        //alert(JSON.stringify($scope.hearing.advocateName));
    //    }
    //    //alert(JSON.stringify($scope.hearing.advocateName));
    //
    //    if($scope.hearingCheck==true) {
    //        $scope.hearingCheck = false;
    //        $http({
    //            method: 'POST',
    //            url: 'api/HearingDates',
    //            headers: {"Content-Type": "application/json", "Accept": "application/json"},
    //            data: $scope.hearing
    //        }).success(function (response) {
    //            console.log('hearing Post Response :' + JSON.stringify(response));
    //            $rootScope.hearingData.push(response);
    //            $scope.hearingCheck = true;
    //            $scope.selectCaseNumber = response.caseNumber;
    //            $scope.caseShow = true;
    //            $scope.caseHide = false;
    //            $('#paymentEdit').modal('hide');
    //            $("#addDesignation").modal("hide");
    //            $('#addDepartmentSuccess').modal('show');
    //            setTimeout(function () {
    //                $('#addDepartmentSuccess').modal('hide')
    //            }, 9000);
    //            $('#createHearingDateModal').modal('hide');
    //        }).error(function (response) {
    //            console.log('Post hearing Error Response :' + JSON.stringify(response));
    //            $scope.hearingCheck = true;
    //
    //        });
    //    }
    //};
    $scope.updateHearingDate = {};
    $scope.edithearingPopup = function (hearingInfo) {
        //console.log('Edit hearing:' + JSON.stringify(hearingInfo));
        $scope.updateHearingDate = hearingInfo;
    };

    $scope.editHearingDate = function () {
        //console.log('Edit hearing1:' + JSON.stringify($scope.updateHearingDate));


        $scope.advocateData = [];

        for(var i=0;i<$scope.updateHearingDate.advocateName.length;i++){
            $scope.advocateData.push(JSON.parse($scope.updateHearingDate.advocateName[i]));
        }
        $scope.updateHearingDate.advocateName = [];

        for(var i=0;i<$scope.advocateData.length;i++){

            $scope.updateHearingDate.advocateName.push({
                name:$scope.advocateData[i].name,
                email:$scope.advocateData[i].email,
                advId:$scope.advocateData[i].advId,
                phoneNumber:$scope.advocateData[i].phoneNumber

            });

// $scope.updateHearingDate.advocateName.push($scope.advocateData[i].advId);

        }
        console.log('Edit hearing Data:' + JSON.stringify($scope.updateHearingDate));
        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
        $scope.updateHearingDate['lastEditPerson'] = loginPersonDetails.employeeId;
        $http({
            method: 'PUT',
            url: 'api/HearingDates/' + $scope.updateHearingDate.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateHearingDate
        }).success(function (response) {
            //console.log('hearing put Response :' + JSON.stringify(response));
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            $('#hearingEdit').modal('hide');
             $window.location.reload();
        }).error(function (response) {
            console.log('hearingDate Error'+JSON.stringify(response));
        });

    };
    $scope.cancelEdit = function () {
        $scope.gethearingDate();
        $('#hearingEdit').modal('hide');
    };
    $scope.reset();

    //*************get CaseNumber*******
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getAdvocates Response :' + JSON.stringify(response));
            $scope.advocates = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getAdvocates();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getStatus Response :' + JSON.stringify(response));
            $scope.status = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getStatus();

    //*********************************File Upload for Hearing Date************************************
    var formIdsArray = [];
    var formUploadStatus = true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        //var count=1;
        //  filedetails=[];
        formIdsArray = [];
        //$scope.docList = [];
        $scope.forms = files;
        var fileCount = 0;
        angular.forEach(files, function (file) {
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });
    };

    //******************* File Upload For Hearing Date *****************************************
    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.hearingData!=null &&  $scope.hearingData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Advocate Name</th>" +
                "<th>Hearing date</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.hearingData.length;i++){
                var paymentData=$scope.hearingData[i];


                $("<tr>" +
                    "<td>"+paymentData.caseNumber+"</td>" +
                    "<td>"+paymentData.advocateName[0].name+"</td>" +
                    "<td>"+paymentData.dateHearing+"</td>" +
                    "<td>"+paymentData.status+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'hearingDate ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);
//****************************hearingDateController **************************************

//****************************StakeHolderController **************************************
app.controller('caseStakeHoldersController', ['legalManagement', '$http', 'Excel', '$timeout', '$scope', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(legalManagement, $http,Excel, $timeout, $scope, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {
    console.log("caseStakeHoldersController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseStakeHolders").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseStakeHolders .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'caseStakeHolder'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(
                		   !response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;

    $scope.getcaseStakeHolders = function () {
        $http({
            "method": "GET",
            "url": 'api/CaseStakeHolders',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getcaseStakeHoldersss  Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseStakeHolderData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getcaseStakeHolders();

    $scope.reset = function () {
//    alert($scope.master);
        $scope.caseStakeHolder = angular.copy($scope.master);
//         $scope.holderCreationError = false;
//                $scope.createHolderTypeError = '';
    };

    $scope.caseStakeClick=function(){
        $('#createCaseStakeModal').modal('show');
        $scope.caseStakeHolder = {};
        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';
    }
    $scope.caseStakeHolder = {};

    $scope.check=true;
    $scope.createMastercaseStakeHolders = function () {


        $scope.holderCreationError = false;
        $scope.createHolderTypeError = '';

      //  var caseNumber = $scope.caseStakeHolder.caseId.caseNumber;
       // var name = $scope.caseStakeHolder.stake.name;
        //$scope.caseStakeHolder['caseNumber']=caseNumber;
        //$scope.caseStakeHolder['name']=name;
//        alert(JSON.stringify(caseNumber))
console.log("==========fdsfdf====="+JSON.stringify($scope.caseStakeHolder.caseId))
//            alert(caseNumber)
            if($scope.caseStakeHolder.caseId!=undefined){
            if($scope.caseStakeHolder.stake!=undefined){
            console.log("==============="+JSON.stringify($scope.caseStakeHolder.caseId))
        if( $scope.check==true) {
            $scope.check=false;
            console.log("============================");
            //if (checkMailFormat($scope.caseStakeHolder.email)) {
        var caseNumber = $scope.caseStakeHolder.caseId.caseNumber;
        var name = $scope.caseStakeHolder.stake.name;
        $scope.caseStakeHolder['caseNumber']=caseNumber;
        $scope.caseStakeHolder['name']=name;

            console.log("post entered"+JSON.stringify($scope.caseStakeHolder));

                $http({
                    method: 'POST',
                    url: 'api/CaseStakeHolders',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.caseStakeHolder
                }).success(function (response) {
               // alert(JSON.stringify(response));
                    $scope.loadingImage=false;
                    $scope.check=true;

                    $rootScope.caseStakeHolderData.push(response);
                    $scope.selectCaseNumber = response.caseNumber;
                    $scope.caseShow = true;
                    $scope.caseHide = false;
                    $('#paymentEdit').modal('hide');
                    $("#addDesignation").modal("hide");
                    $('#addDepartmentSuccess').modal('show');
                    setTimeout(function () {
                        $('#addDepartmentSuccess').modal('hide')
                    }, 9000);


                    $('#createCaseStakeModal').modal('hide');
                }).error(function (response) {
                    console.log('Post caseStakeHolder Error Response :' + JSON.stringify(response));
                    if (response.error.message) {
                        $scope.createHolderTypeError = response.error.message;
                        $scope.holderCreationError = true;
                        $scope.check=true;
                    }
                });


        }

        }else{

         $scope.holderCreationError = true;
          $scope.createHolderTypeError = 'Please Select Stake Holder Name';
          $scope.check=true;

        }}else{

                  $scope.holderCreationError = true;
                   $scope.createHolderTypeError = 'Please Select Case Number';
                   $scope.check=true;

                 }
           };
    $scope.updateCaseStakeHolder = {};
    $scope.editCaseStakeHolderPopup = function (caseStakeHolderInfo) {
        //console.log('Edit updatecaseStakeHolder:' + JSON.stringify(caseStakeHolderInfo));
        $scope.updateCaseStakeHolder = caseStakeHolderInfo;
    };

    $scope.editCaseStakeHolder = function () {
        //console.log("updateCaseStakeHolder::::::::::"+JSON.stringify($scope.updateCaseStakeHolder))
        $scope.holderUpdationError = false;
        $scope.updateHolderTypeError = '';



        var caseNumber = $scope.updateCaseStakeHolder.caseId.caseNumber;
        var name = $scope.updateCaseStakeHolder.stake.name;
        $scope.updateCaseStakeHolder['caseNumber']=caseNumber;
        $scope.updateCaseStakeHolder['name']=name;



        $http({
            method: 'PUT',
            url: 'api/CaseStakeHolders/' + $scope.updateCaseStakeHolder.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateCaseStakeHolder
        }).success(function (response) {
         console.log('caseStakeHolderUpdate: '+JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            //$('#myModal').modal('hide');
            $('#caseStakeHolderEdit').modal('hide');
        }).error(function (response) {
            console.log('caseStakeHolderUpdate Error'+JSON.stringify(response));
            //if(response.error.message) {
            //    $scope.updateHolderTypeError = response.error.message;
            //    $scope.holderUpdationError = true;
            //}
        });

    };
    $scope.cancelEdit = function () {
        $scope.getcaseStakeHolders();
        $('#caseStakeHolderEdit').modal('hide');
    };



    //*************get CaseNumber*******
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();

    $scope.getStakeHolders = function () {
        $http({
            method: 'GET',
            url: 'api/StakeHolders',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getStakeHolders Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.stakeHolders = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getStakeHolders();

    $scope.getDepartments = function (adv) {
        $http({
            method: 'GET',
            url: 'api/StakeHolders?filter={"where":{"name":"'+adv+'"}}',
            //url: 'api/StakeHolders',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('******************getdepartments Response :' + JSON.stringify(response));
           // alert('v '+JSON.stringify(response))
            $scope.departments = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };

    /*$scope.getDesignation = function (adv) {
        $http({
            method: 'GET',
            url: 'api/StakeHolders?filter={"where":{"name":"'+adv+'"}}',
            //url: 'api/StakeHolders',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('******************getdepartments Response :' + JSON.stringify(response));
            // alert('v '+JSON.stringify(response))
            $scope.designation = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };*/

//    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.caseStakeHolderData!=null &&  $scope.caseStakeHolderData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Case StakeHolder name</th>" +
                "<th>Email</th>" +

                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.caseStakeHolderData.length;i++){
                var paymentData=$scope.caseStakeHolderData[i];


                $("<tr>" +
                    "<td>"+paymentData.caseNumber+"</td>" +
                    "<td>"+paymentData.name+"</td>" +
                    "<td>"+paymentData.email+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'caseStakeHolders ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
//****************************caseStakeHoldersController **************************************
//****************************noticeCommentsController **************************************
app.controller('noticeCommentsController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope,Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("noticeCommentsController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'noticeComments'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.getNotice = function () {
        $http({
            "method": "GET",
            "url": 'api/NoticeComments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Notice Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.noticeData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getNotice();

    if($window.localStorage.getItem('noticeCreationCheck')){
        $scope.caseShow=true;
        $scope.caseHide=false;
        $scope.selectedCaseNumber = $window.localStorage.getItem('noticeCreationCheck')
        $window.localStorage.removeItem('noticeCreationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }


    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };


    $scope.updateNotice = {};
    $scope.editNoticePopup = function (noticeInfo) {
        //console.log('Edit noticeInfo:' + JSON.stringify(noticeInfo));
        $scope.updateNotice = noticeInfo;
    };

    $scope.editNotice = function () {
        //console.log('Edit Notice1:' + JSON.stringify($scope.updateNotice));
//         var caseNumber = $scope.updateNotice.caseId.caseNumber;
//          $scope.updateNotice['caseNumber']=caseNumber;

        $http({
            method: 'PUT',
            url: 'api/NoticeComments/' + $scope.updateNotice.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateNotice
        }).success(function (response) {
            //console.log('updateNotice Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $('#paymentEdit').modal('hide');
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            $('#noticeEdit').modal('hide');
        }).error(function (response) {
            console.log('noticeUpdate Error');
        });

    };
    $scope.cancelEdit = function () {
        $scope.getNotice();
        $('#noticeEdit').modal('hide');
    };


    $scope.reset();


    //*************get CaseNumber*******
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.noticeData!=null &&  $scope.noticeData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Notice Number</th>" +
                "</tr>").appendTo("table"+tableId);
            for(var i=0;i<$scope.noticeData.length;i++){
                var paymentData=$scope.noticeData[i];
                $("<tr>" +
                    "<td>"+paymentData.caseNumber+"</td>" +
                    "<td>"+paymentData.noticeNumber+"</td>" +
                    "</tr>").appendTo("table"+tableId);            }
            var exportHref = Excel.tableToExcel(tableId, 'noticeComments ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);
//****************************noticeCommentsController **************************************

//**************************caseController*******************

app.controller('caseController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', 'Excel', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, Upload, $timeout,Excel, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
    console.log("caseController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    //************Get Method****************
    $scope.getCase = function () {
        $http({
            "method": "GET",
            "url": 'api/CreateCases',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get create case  Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.CaseData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getCase();


    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.case = {};

    $scope.getEditCase=function (caseInfo) {

        //console.log("getEditCase:::" +JSON.stringify(caseInfo));
        $rootScope.updateCase=caseInfo;
    }
    if($window.localStorage.getItem('caseCreationCheck')){

             $scope.caseShow=true;
             $scope.caseHide=false;

            $scope.selectedCaseNumber = $window.localStorage.getItem('caseCreationCheck')
             $window.localStorage.removeItem('caseCreationCheck')
               // alert("hiii");
             $("#addDesignation").modal("hide");
             $('#addDepartmentSuccess').modal('show');
             setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 90000);
              $window.location.reload();
            }

    if($window.localStorage.getItem('caseUpdationCheck')){
        $scope.caseShow=false;
        $scope.caseHide=true;
        $scope.selectedCaseNumb = $window.localStorage.getItem('caseUpdationCheck')
        $window.localStorage.removeItem('caseUpdationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }
    //$scope.addDepartmentSuccessModal = function() {
    //    //alert("addDepartmentSuccessModal");
    //    $('#addDepartmentSuccess').modal('hide');
    //    $window.location.reload();
    //    location.href = '#/case';
    //}

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.CaseData!=null &&  $scope.CaseData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Case Type</th>" +
                "<th>Advocate Name</th>" +
                "<th>Status</th>" +
                "<th>Priority</th>" +
                "<th>Act Number</th>" +
                "<th>Case Created date</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.CaseData.length;i++){
                var caseData=$scope.CaseData[i];
        console.log("!!!!!!!!!!!!!!!!111!"+JSON.stringify(caseData.advocate.length))

                $("<tr>" +
                    "<td>"+caseData.caseNumber+"</td>" +
                    "<td>"+caseData.caseType+"</td>" +
                    "<td>"+caseData.advocate[0].name+"</td>" +
                    "<td>"+caseData.status+"</td>" +
                    "<td>"+caseData.priority+"</td>" +
                    "<td>"+caseData.act+"</td>" +
                    "<td>"+caseData.createdTime+"</td>" +

                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'case ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }


}]);

//**********caseController******

//***********caseDetailsController controller**************************************
app.controller('caseDetailsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q) {
    console.log("caseDetailsController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                    		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {
    //    					alert(JSON.stringify(response));
                    		   if(!response){
                    			window.location.href = "#/noAccessPage";
                    		   }
                    		});

    //$scope.docUploadURL = 'apiUploads/dhanbadDb/download/';
    $scope.dtOptions = {paging: false, searching: false};
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('CaseNumber  :' + JSON.stringify(response));
            $rootScope.caseList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getCaseNumber();


    //******file Upload***********
    var formIdsArray = [];
    var formUploadStatus = true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        //var count=1;
        //  filedetails=[];
        formIdsArray = [];
        //$scope.docList = [];
        $scope.forms = files;

        angular.forEach(files, function (file) {
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });
    };
//******file Upload***********

    $scope.caseUploads = function () {
        // //alert('hai');
        if (formUploadStatus) {
            var uploadDetails = $scope.upload;
            uploadDetails.file = $scope.docList;
            var caseNumber = $scope.selectCaseNumber;

            uploadDetails['caseNumber'] = caseNumber;
            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            uploadDetails['createdPerson'] = loginPersonDetails.name;
            uploadDetails['uploadedPersonName'] = loginPersonDetails.name;
            //  //alert('uplaoded data is'+JSON.stringify($scope.docList));
            uploadDetails.status = 'active';
            $http({
                method: 'POST',
                url: 'api/CaseUploads',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                "data": uploadDetails
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                $("#scroll2").hide();
                $("#scroll").show();
                $scope.upload = {};
                $scope.docList = [];
                $scope.getDocuments();
            }).error(function (response) {
                //console.log('Error Response :' + JSON.stringify(response));
            });
            //console.log('scheme details are'+JSON.stringify(schemeDetails));
        } else {
            $scope.createScheme();
        }
    }

    $scope.docUploadURL = uploadFileURL;

    $scope.getDocuments = function () {
     console.log("case number"+$scope.selectCaseNumber);
        $http({
            method: 'GET',
            url: 'api/CaseUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.documentsList = response;

            console.log(" $scope.documentsList"+ JSON.stringify($scope.documentsList));
            console.log(" $scope.documentsList"+ JSON.stringify(response));
            //   //alert('hai'+response.length);
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    //***********select*************

    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('filter for case number :' + JSON.stringify(response));
            $scope.selectCaseDetails = response[0];

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

        $scope.getDocuments();

    }


    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
    })
}]);

//*********** caseDetailsController controller**************************************


//*********** advocatePaymentDetailsController**************************************
app.controller('advocatePaymentDetailsController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
    console.log("advocatePaymentDetailsController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'advocatePayments'}, function (response) {
//        alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.getAdvocate = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           // console.log('getAdvocate  :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $rootScope.advoatesList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getAdvocate();

    $scope.selectareas = function (name) {
        $scope.selectadvocateName = name;
        console.log("=======================::"+JSON.stringify($scope.selectadvocateName))

        $http({
            method: 'GET',
            url: 'api/AdvocatePayments/?filter={"where":{"advocateName":"' + $scope.selectadvocateName + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
           //console.log('filter for advocateName :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.selectAdvocateDetails = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }


}]);
//*********** advocatePaymentDetailsController**************************************

//*********** noticeDetailsController**************************************
app.controller('noticeDetailsController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
    console.log("noticeDetailsController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'noticeComments'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

     $scope.generatePDF = function() {
                var printContents = document.getElementById('formConfirmation').innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();

                document.body.innerHTML = originalContents;

            }
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/NoticeComments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('notice comments response  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.noticeList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getCaseNumber();

    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;

        $http({
            method: 'GET',
            url: 'api/NoticeComments/?filter={"where":{"noticeNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter for case number :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectCaseDetails = response[0];

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }

    //function myFunction() {
    //    alert(hii)
    //    window.print();
    //}


}]);
//*********** noticeDetailsController**************************************
//*********** hearingDetailsController**************************************
app.controller('hearingDetailsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', '$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,$routeParams, DTOptionsBuilder, DTColumnBuilder) {
    console.log("hearingDetailsController");
    //$scope.docUploadURL = 'apiUploads/dhanbadDb/download/';
    $scope.dtOptions = {paging: false, searching: false};


$(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'hearingDateInfo'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.deleteFile = function(fileId, index){
        console.log('DocList Before:'+JSON.stringify($scope.docList));
        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
            //console.log('DocList After:'+JSON.stringify($scope.docList));
        });
    }

    //******file Upload***********
    var formIdsArray = [];
    var formUploadStatus = false;
    $scope.errorMssg1 = false;
    $scope.fileUploadSuccess = false;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        //var count=1;
        //  filedetails=[];
        console.log("hearing upload docs entered");
        formIdsArray = [];
        //$scope.docList = [];
        //$scope.forms = files;
        var fileCount = 0;
        angular.forEach(files, function (file) {
            $scope.errorMssg1 = true;
            $scope.fileUploadSuccess = false;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.errorMssg1 = false;
                $scope.fileUploadSuccess = true;
                $scope.uploadError = false;
                $scope.uploadErrorMessage = '';
            }
        });
    };
//******file Upload***********
    $scope.uploadError = false;
    $scope.uploadErrorMessage = '';
    $scope.caseUploads = function () {
         console.log('hai caseupload function');
        if (formUploadStatus) {
            if($scope.upload && $scope.upload.comments) {
                var uploadDetails = $scope.upload;
                uploadDetails.file = $scope.docList;
                var caseNumber = $scope.selectCaseNumber;
                uploadDetails['caseNumber'] = caseNumber;
                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                uploadDetails['createdPerson'] = loginPersonDetails.name;
                uploadDetails['uploadedPersonName'] = loginPersonDetails.name;
                //  //alert('uplaoded data is'+JSON.stringify($scope.docList));
                uploadDetails.status = 'active';
                $http({
                    method: 'POST',
                    url: 'api/HearingUploads',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": uploadDetails
                }).success(function (response) {
                    console.log('hearing caseupload function Response :' + JSON.stringify(response));
                    $("#scroll2").hide();
                    $("#scroll").show();
                    $scope.errorMssg1 = false;
                    $scope.fileUploadSuccess = false;
                    $scope.uploadError = false;
                    $scope.uploadErrorMessage = '';
                    $scope.upload = {};
                    $scope.docList = [];
                    $scope.getDocuments();
                }).error(function (response) {
                    //console.log('Error Response :' + JSON.stringify(response));
                });
                //console.log('scheme details are'+JSON.stringify(schemeDetails));
            }else {
                $scope.uploadError = true;
                $scope.uploadErrorMessage = 'Please enter the comment';
            }
        } else {
            $scope.uploadError = true;
            $scope.uploadErrorMessage = 'Please attach the files.';
            //$scope.createScheme();
        }
    }
    $scope.docUploadURL = uploadFileURL;

    $scope.getDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/HearingUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.documentsList = response;
               console.log('hai docs list'+response.length);
            console.log(" $scope.documentsList"+ JSON.stringify($scope.documentsList));
            console.log(" $scope.documentsList"+ JSON.stringify(response));
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    //***********select*************
$scope.cases= $window.localStorage.getItem('caseNumbb');
    //alert($scope.cases);
    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;
        $scope.eventResponse = JSON.parse($window.localStorage.getItem('events'));
        //alert($routeParams.id);

            //alert("*********dateEdit 3092: ***********: " + JSON.stringify($scope.eventResponse[1].start));
        var url;
                if($routeParams.id){
             url= 'api/HearingDates/?filter={"where":{"id":"' + $routeParams.id + '"}}'
                    }else{
             url= 'api/HearingDates/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}'
                }
            $http({
                method: 'GET',
                url: url,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                //alert(JSON.stringify(response));
                $scope.selectCaseDetails = response[0];
                $window.localStorage.setItem('hearingDetails', $scope.selectCaseDetails);
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });

            $scope.getDocuments();
            $scope.selectHearingHistory();
        }


    $scope.selectHearingHistory = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/HearingHistories/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter selectHearingDetails :' + JSON.stringify(response));
            $scope.selectHearingHistoryDetails = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    //alert($routeParams.id);
    //$scope.caseId = $routeParams.id;


    $scope.getCaseNumber = function (caseId) {
        var url;
        if(caseId){
            url = 'api/HearingDates/?filter={"where":{"id":"' + caseId + '"}}';
        }else{
            url = 'api/HearingDates';
        }
        $http({
            method: 'GET',
            url: url,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('CaseNumber  :' + JSON.stringify(response));
            //var caseListSet = new Set();
            $rootScope.caseList = response;

            //for(var j=0;j<response.length;j++){
            //    caseListSet.add(response[j].caseNumber);
            //    //caseListSet.add(response[j].editDate);
            //
            //}
            //$scope.caseNumberList = Array.from(caseListSet);
            //alert('CaseNumber  :' + JSON.stringify($scope.caseNumberList));

            if(caseId){
                $scope.selectareas(response[0].caseNumber);
                $scope.caseIdShow =false;
            }
            else{
                $scope.caseIdShow =true;
            }

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
            //alert('Error Response :' + JSON.stringify(response));
        });


    }
    if($routeParams.id) {
        $scope.getCaseNumber($routeParams.id);
    }else{
        $scope.getCaseNumber();
    }
    $scope.reset = function () {
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;

    };

    $scope.cancel=function(){
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
    }

    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
    });

$scope.reset();
}]);
//*********** hearingDetailsController***********************************
//***********edit case controller**************************************

app.controller('editCaseController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {
    console.log("editCaseController Entered..."+JSON.stringify($rootScope.updateCase));

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {
//            alert(JSON.stringify(response));
               if(!response){
                window.location.href = "#/noAccessPage";
               }
            });


    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    $scope.getCase = function () {
        $http({
            "method": "GET",
            "url": 'api/CreateCases',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get create case  Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $rootScope.CaseData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getCase();


    $scope.getCaseTypes = function () {
        $http({
            method: 'GET',
            url: 'api/CaseTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('GetCaseType Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.caseTypes = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseTypes();

    $scope.getDepartment = function () {
        $http({
            method: 'GET',
            url: 'api/caseDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('caseDepartments Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.department = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getDepartment();


    $scope.getEmapnnel = function () {
        $http({
            method: 'GET',
            url: 'api/EmpannelGroups',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getEmapnnel Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.empannels = response;
            $scope.getAdvocates($rootScope.updateCase.empannel);

            if($rootScope.updateCase) {
                $scope.getAdvocates($rootScope.updateCase.empannel);
            }

        }).error(function (response) {
            //console.log('Error getEmapnnel Response :' + JSON.stringify(response));
        });
    };
    $scope.getEmapnnel();

    $scope.getAdvocates = function (emp) {
        //alert(emp);
        $http({
            method: 'GET',
            url: 'api/EmpannelGroups?filter={"where":{"groupName":"'+emp+'"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //for(var i=0; i< $scope.updateCase.advocate.length; i++){
            //    delete  $scope.updateCase.advocate[i]['$$hashKey'];
            //}
            //console.log('Advocate edit:'+JSON.stringify($rootScope.updateCase.advocate));
            // console.log('getadvocates Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.advocates = response[0].advocateList;
        }).error(function (response) {
            console.log('Error getEmapnnel Response :' + JSON.stringify(response));
        });
    };
     //$scope.getAdvocates();



    $scope.getCourt = function () {
        $http({
            method: 'GET',
            url: 'api/Courts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('courtName Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.courtName = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCourt();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getStatus Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.status = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getStatus();

    $scope.getPriority = function () {
        $http({
            method: 'GET',
            url: 'api/Priorities',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getPriority Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.priority = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getPriority();

    $scope.getActs = function () {
        $http({
            method: 'GET',
            url: 'api/Acts',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getActs Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.acts = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getActs();


      //********************************Edit Create Case*********************

/*
    $scope.updateCase = {};
    $scope.editCase = function (caseInfo) {
        console.log('Edit updatecase:' + JSON.stringify(caseInfo));
        $scope.updateCase = caseInfo;
        console.log("$scope.updateCase"+JSON.stringify($scope.updateCase));
    };
*/


    $scope.editCase = function () {
        //console.log('Edit case1:' + JSON.stringify($rootScope.updateCase));


        $scope.advocateData = [];
        for(var i=0;i<$scope.updateCase.advocate.length;i++){
            //console.log('Type of Advocate data:'+(typeof $scope.updateCase.advocate[i]));
            if((typeof $scope.updateCase.advocate[i]) == 'object'){
                $scope.advocateData.push($scope.updateCase.advocate[i]);
            }else if((typeof $scope.updateCase.advocate[i]) == 'string'){
                $scope.advocateData.push(JSON.parse($scope.updateCase.advocate[i]));
            }
            //alert("2780"+JSON.stringify($scope.advocateData));
        }
        $scope.updateCase.advocate = [];

        for(var i=0;i<$scope.advocateData.length;i++){
            //alert("in for");
            $scope.updateCase.advocate.push({
                name:$scope.advocateData[i].name,
                email:$scope.advocateData[i].email,
                advId:$scope.advocateData[i].advId,
                phoneNumber:$scope.advocateData[i].phoneNumber
            });
            //alert("2790"+JSON.stringify($scope.updateCase.advocate.name));
        }
        //console.log('Edit case111111:' + JSON.stringify($rootScope.updateCase));
        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
        $scope.updateCase['lastEditPerson'] = loginPersonDetails.employeeId;

        $http({
            method: 'PUT',
            url: 'api/CreateCases/' + $rootScope.updateCase.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $rootScope.updateCase
        }).success(function (response) {
           // console.log('updatecase Response :' + JSON.stringify(response));
            $scope.loadingImage = false;
            $scope.selectCaseNumb=response.caseNumber;
            $window.localStorage.setItem('caseUpdationCheck', $scope.selectCaseNumb);
            location.href = '#/case';
            $window.location.reload();

        }).error(function (response) {
            console.log('caseUpdate Error');
            console.log("caseUpdate Error"+JSON.stringify(response));
        });

    };
    $scope.cancelEdit = function () {
        $scope.getCase();
        $('#caseEdit').modal('hide');
    };
    //$scope.addCaseSuccessModal = function() {
    //    alert("addDepartmentSuccessModal");
    //    $('#editCaseSuccess').modal('hide');
    //    $window.location.reload();
    //    location.href = '#/case';
    //}

    $scope.checkAdvocateSelected = function(advocate){
        var check;
        for(var i=0; i < $rootScope.updateCase.advocate.length; i++) {
            if ($rootScope.updateCase.advocate[i].email == advocate.email) {
                check = true;
                break;
            }
        }

        if(check){
            return true;
        }else{
            return false;
        }
    };

}]);
//***********edit case controller

//*************************caseUploadController*****************************/
app.controller('caseUploadController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {
    console.log("caseUploadController");

    $scope.uploadFile = uploadFileURL;

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseUpload").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseUpload .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'docUpload'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.dtOptions = {paging: false, searching: false};
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('CaseNumber  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getCaseNumber();


    //******file Upload***********
    var formIdsArray = [];
    var formUploadStatus = false;
    $scope.errorMssg1 = false;
    $scope.fileUploadSuccess = false;
    $scope.uploadDocuments = function (files) {
        //var count=1;
        //  filedetails=[];
        formIdsArray = [];
        //$scope.docList = [];
        $scope.forms = files;
        var fileCount = 0;

        angular.forEach(files, function (file) {
            $scope.errorMssg1 = true;
            $scope.fileUploadSuccess = false;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.errorMssg1 = false;
                $scope.fileUploadSuccess = true;
                $scope.uploadError = false;
                $scope.uploadErrorMessage = '';
            }
        });
    };
//******file Upload***********
    $scope.uploadError = false;
    $scope.uploadErrorMessage = '';
    $scope.caseUploads = function () {
        // //alert('hai');
        if (formUploadStatus) {
            if($scope.upload && $scope.upload.comments) {
                var uploadDetails = $scope.upload;
                uploadDetails.file = $scope.docList;
                var caseNumber = $scope.selectCaseNumber;
                uploadDetails['caseNumber'] = caseNumber;
                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                uploadDetails['createdPerson'] = loginPersonDetails.name;
                uploadDetails['uploadedPersonName'] = loginPersonDetails.name;
                //  //alert('uplaoded data is'+JSON.stringify($scope.docList));
                uploadDetails.status = 'active';
                $http({
                    method: 'POST',
                    url: 'api/CaseUploads',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": uploadDetails
                }).success(function (response) {
                    //console.log('Users Response :' + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $("#scroll2").hide();
                    $("#scroll").show();
                    $scope.errorMssg1 = false;
                    $scope.fileUploadSuccess = false;
                    $scope.uploadError = false;
                    $scope.uploadErrorMessage = '';
                    $scope.upload = {};
                    $scope.docList = [];
                    $scope.getDocuments();
                }).error(function (response) {
                    //console.log('Error Response :' + JSON.stringify(response));
                });
                //console.log('scheme details are'+JSON.stringify(schemeDetails));
            }else{
                $scope.uploadError = true;
                $scope.uploadErrorMessage = 'Please enter the comment';
            }
        } else {
            $scope.uploadError = true;
            $scope.uploadErrorMessage = 'Please attach the files.';
            //$scope.createScheme();
        }
    }

  //**********************hearing uploads*****************************


    //******file Upload***********
    $scope.uploadHearingError = false;
    $scope.uploadHearingErrorMessage = '';
    $scope.hearingUploads = function () {
        // //alert('hai');
        if (formUploadStatus) {
            if($scope.upload && $scope.upload.comments) {
                var uploadHearingDetails = $scope.upload;
                uploadHearingDetails.file = $scope.docList;
                var caseNumbers = $scope.selectCaseNumber;
                uploadHearingDetails['caseNumber'] = caseNumbers;
                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                uploadHearingDetails['createdPerson'] = loginPersonDetails.name;
                uploadHearingDetails['uploadedPersonName'] = loginPersonDetails.name;
                //  //alert('uplaoded data is'+JSON.stringify($scope.docList));
                uploadHearingDetails.status = 'active';
                $http({
                    method: 'POST',
                    url: 'api/HearingUploads',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": uploadHearingDetails
                }).success(function (response) {
                    //console.log('Users Response :' + JSON.stringify(response));
                    $scope.loadingImage=false;
                    $("#scroll22").hide();
                    $("#scroll").show();
                    $scope.errorMssg12 = false;
                    $scope.fileUploadSuccesss = false;
                    $scope.uploadHearingError = false;
                    $scope.uploadHearingErrorMessage = '';
                    $scope.upload = {};
                    $scope.docList = [];
                    $scope.getHearingDocs();
                }).error(function (response) {
                    //console.log('Error Response :' + JSON.stringify(response));
                });
                //console.log('scheme details are'+JSON.stringify(schemeDetails));
            }else{
                $scope.uploadHearingError = true;
                $scope.uploadHearingErrorMessage = 'Please enter the comment';
            }
        } else {
            $scope.uploadHearingError = true;
            $scope.uploadHearingErrorMessage = 'Please attach the files.';
            //$scope.createScheme();
        }
    }

    //**********************hearing uploads*****************************

    $scope.deleteFile = function(fileId, index){
        //alert("TTTTT"+fileId);
        console.log('DocList Before:'+JSON.stringify($scope.docList));
        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.docList.splice(index, 1);
            //console.log('DocList After:'+JSON.stringify($scope.docList));
        });
    }


    $scope.docUploadURL = uploadFileURL;
    $scope.getDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/CaseUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.documentsList = response;
            $scope.loadingImage=false;
            //   //alert('hai'+response.length);
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    //************HearingDocuments***************

    $scope.getHearingDocs = function () {

        $http({
            method: 'GET',
            url: 'api/HearingUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.hearingDocsList = response;
            $scope.loadingImage=false;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }
    //************HearingDocuments***************

    //***********select*************

    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter for case number :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectCaseDetails = response[0];

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

        $scope.getDocuments();
        $scope.getHearingDocs();

    }


    $scope.reset = function () {

        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;
    };

    $scope.cancel=function(){
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;
        $("#cancel").click(function () {
            $("#scroll2").hide()
            //$("#scroll22").hide()
            $("#scroll").show();
            //$("#scroll1").show();
            ;
        });
        $("#cancell").click(function () {

            $("#scroll22").hide()

            $("#scroll1").show();
            ;
        });
    }

    $(function () {
        $("#scroll2").hide();
        $("#scroll22").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });

        $("#scroll1").click(function () {
            $("#scroll22").show();
            $("#scroll1").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll22").offset().top
                },
                'slow');
        });

        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });
        $("#cancell").click(function () {
            $("#scroll22").hide()
            $("#scroll1").show();
            ;
        });
    })
$scope.reset();
}]);
//************************caseUploadController******************************/

//****************************createPaymentsController **************************************
app.controller('createPaymentsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
    console.log("createPaymentsController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.advocatePayment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'paymentMode'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var moneyNumeric= /^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$/;
    //*************get CaseNumber*******
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('get creasecase Response =============:' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();





$scope.paymentMode=["Cash","Cheque","DD"];


    $scope.reset = function () {
        $scope.payment = angular.copy($scope.master);
    };

    $scope.payment = {};
    $scope.paymentCheck=true;
   // console.log("create payment function entered");


    $scope.createPayment = function () {
$scope.createPaymentError=false;
$scope.createPaymentMessage='';

$scope.advocateData = [];
$scope.advocateData.push(JSON.parse($scope.payment.advocateName));
$scope.payment.advocateName=[];

for(var i=0;i<$scope.advocateData.length;i++){


    $scope.payment.advocateName.push($scope.advocateData[i].advId);

}


        $scope.selectCaseNumber=$scope.payment.caseNumber;
        console.log("$scope.selectCaseNumber$scope.selectCaseNumber"+$scope.selectCaseNumber);
        var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));


        $scope.payment['createdPerson'] = loginPersonDetails.employeeId;
   //alert(JSON.stringify($scope.payment))
   if($scope.payment.date !=undefined){
        if($scope.paymentCheck==true) {
            $scope.paymentCheck = false;

            //alert($scope.payment.date)
                $http({
                    method: 'POST',
                    url: 'api/AdvocatePayments',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.payment
                }).success(function (response) {
                    //$rootScope.paymentData.push(response);
                    console.log("advocatePayments::::"+JSON.stringify(response))
                    $scope.loadingImage=false;
                    $scope.paymentCheck == true;
                    $scope.selectAdvocate = response.advocateName;
                    $scope.selectCaseNumber = response.caseNumber;
                    $window.localStorage.setItem('CreationCheck', $scope.selectAdvocate);
                    $window.localStorage.setItem('paymentCreationCheck', $scope.selectCaseNumber);

                    location.href = '#/advocatePayment';
                      $window.location.reload();
                }).error(function (response) {
                    console.log('Error Response1 :' + JSON.stringify(response));
                    $scope.paymentCheck == true;
                });
                }
            }
            else{
            alert("hii")
        $scope.createPaymentError=true;
        $scope.createPaymentMessage='Please Select Payment Payment Date';
         $scope.paymentCheck == true;
        }


//       } else{
//        $scope.createPaymentError=true;
//        $scope.createPaymentMessage='Please Select Payment Payment Date';
//         $scope.paymentCheck == true;
//        }
    };


    $scope.getCaseNumber();

    $scope.modeChange=function (paymentMode) {

        console.log("paymentModes change" + JSON.stringify(paymentMode));

        if(paymentMode=="Cheque" || paymentMode=="DD" )
        {
            $scope.onCash=true;
        }
        else {
            $scope.onCash=false;
        }

    }

    $scope.getAdvocates = function () {
            $http({
                method: 'GET',
                url: 'api/Advocates',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                //console.log('getAdvocates Response :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $scope.advocates = response;
                //alert('getAdvocates Response :' + JSON.stringify(response))
            }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
        };
        $scope.getAdvocates();

    $scope.reset();


}]);
//****************************createPaymentsController **************************************

//****************************courtController **************************************
app.controller('courtController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder' , function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {
console.log("courtController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.court").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.court .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'courtInfo'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.getCourts = function () {
        $http({
            "method": "GET",
            "url": 'api/Courts',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.courtData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getCourts();
    $scope.reset = function () {
        $scope.court = angular.copy($scope.master);
        $scope.courtCreationError = false;
        $scope.createCourtTypeError = '';
    };
    $scope.courtClick=function(){
        $('#createCourtModal').modal('show');
        $scope.court = {};
        $scope.courtCreationError = false;
        $scope.createCourtTypeError = '';
    }
    $scope.court = {};
    $scope.courtCheck=true;
    $scope.createCourts = function () {

        $scope.courtCreationError = false;
        $scope.createCourtTypeError = '';

        if($scope.courtCheck==true) {
            $scope.courtCheck=false;
            if ($scope.court.name.match(alphabetsWithSpaces) && $scope.court.name) {


                $http({
                    method: 'POST',
                    url: 'api/Courts',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.court
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.courtCheck=true;
                    $rootScope.courtData.push(response);
                    //$scope.court = {};
                    $('#createCourtModal').modal('hide');
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                    if (response.error.details.messages.name) {
                        $scope.createCourtTypeError = response.error.details.messages.name[0];
                        $scope.courtCreationError = true;
                        $scope.courtCheck=true;
                    }

                });
            } else {
                $scope.createCourtTypeError = 'Please Enter the Court Name in the Alphabets';
                $scope.courtCreationError = true;
                $scope.courtCheck=true;

            }
        }
    };
    $scope.updateCourts = {};
    $scope.editCourtPopup = function (courtInfo) {
        //console.log('Edit CaseTypee:' + JSON.stringify(caseTypeInfo));
        $scope.updateCourts = courtInfo;
        console.log("update case type " + JSON.stringify($scope.updateCourts));
    };

    $scope.editCourts = function () {
        $scope.courtUpdationError = false;
        $scope.updateCourtTypeError = '';
        //if ($scope.updateCaseType.caseNumber) {
        //if ($scope.updateCaseType.caseNumber.match(alphaNumeric) && $scope.updateCaseType.caseNumber) {
        //if ($scope.updateCaseType.caseType) {
        console.log("hiiii");
        console.log('updateCourts :' + JSON.stringify($scope.updateCourts));
        //if ($scope.updateCourts.name.match(alphabetsWithSpaces) && $scope.updateCourts.name) {
            if ($scope.updateCourts.name.match(alphabetsWithSpaces)) {
            console.log('Edit caseType:' + JSON.stringify($scope.updateCourts));

            $http({
                method: 'PUT',
                url: 'api/Courts/' + $scope.updateCourts.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updateCourts
            }).success(function (response) {
                //console.log('updateCaseType Response :' + JSON.stringify(response));
                $scope.loadingImage=false;
                //$('#myModal').modal('hide');
                $('#caseTypeEdit').modal('hide');
            }).error(function (response) {
                console.log('court Error');
                console.log("response Error: "+JSON.stringify(response));
                if (response.error.details.messages.name) {
                    $scope.updateCourtTypeError = response.error.details.messages.name[0];
                    $scope.courtUpdationError = true;
                }

                $timeout(function (){
                    $scope.courtUpdationError = false;
                },3000)
            });
        } else {
            $scope.updateCourtTypeError = 'Please Enter the Court Name in Alphabets';
            $scope.courtUpdationError = true;
            $timeout(function (){
                $scope.courtUpdationError = false;
            },3000)

        }
        //} else {
        //    $scope.updateCaseTypeError = 'Please enter the Case Number in the Correct format';
        //    $scope.caseTYpeUpdationError = true;
        //
        //}

    };
    $scope.cancelEdit = function () {
        $scope.getCourts();
        $('#caseTypeEdit').modal('hide');
    };

    $scope.reset();
    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.courtData!=null &&  $scope.courtData.length>0){

            $("<tr>" +
                "<th>Court Name</th>" +
                "<th>Court Jurisdiction</th>" +
                "<th>City</th>" +
                "<th>Address</th>" +



                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.courtData.length;i++){
                var paymentData=$scope.courtData[i];


                $("<tr>" +
                    "<td>"+paymentData.name+"</td>" +
                    "<td>"+paymentData.jurisdiction+"</td>" +
                    "<td>"+paymentData.city+"</td>" +
                    "<td>"+paymentData.address+"</td>" +



                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'court');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);
//****************************courtController **************************************

//******************************createNoticeController ************************************
app.controller('createNoticeController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder) {
    console.log("createNoticeController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.noticeComments .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'noticeComments'}, function (response) {
//					alert(JSON.stringify(response));
            		   if(!response){
            			window.location.href = "#/noAccessPage";
            		   }
            		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

    $scope.getNotice = function () {
        $http({
            "method": "GET",
            "url": 'api/NoticeComments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Notice Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.noticeData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getNotice();


    /* multi comment starts*/

    $scope.choiceSet = {choices: []};

    $scope.choiceSet.choices = [];
    $scope.addNewChoice = function () {
        $scope.choiceSet.choices.push('');
    };

    $scope.removeChoice = function (z) {
        //var lastItem = $scope.choiceSet.choices.length - 1;
        $scope.choiceSet.choices.splice(z,1);
    };



    $scope.createNotice = function () {
    $scope.createNoticeError= false;
    $scope.createNoticeMessage = '';

        var comment=$scope.choiceSet.choices;

        $scope.notice["comment"]=comment;


        //$scope.notice.comment=comment;
        //console.log("in notice: "+JSON.stringify($scope.notice));
        if($scope.choiceSet.choices == ''){
            alert("Please Enter Comment");
        }else{
        console.log("00000000000000000000000: "+JSON.stringify($scope.notice));
//        console.log("0000000000000000000eeeee0000: "+JSON.stringify($scope.caseId));
//        if($scope.caseId!=undefined){
//        var caseNumber = $scope.caseId.caseNumber;
//        $scope.notice['caseNumber']=caseNumber;

            $http({
                method: 'POST',
                url: 'api/NoticeComments',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.notice
            }).success(function (response) {
            console.log(JSON.stringify(response))
                $scope.loadingImage=false;
                $rootScope.noticeData.push(response);
                $scope.selectCaseNumber=response.caseNumber;
                $window.localStorage.setItem('noticeCreationCheck',$scope.selectCaseNumber);

                location.href = '#/noticeComments';
            }).error(function (response) {
                console.log('Post Notice Error Response :' + JSON.stringify(response));
                if(response.error.details.messages.noticeNumber[0]){
                 $scope.createNoticeError= true;
                 $scope.createNoticeMessage = response.error.details.messages.noticeNumber[0];
                }


        });
//             }else{
//                              $scope.createNoticeError= true;
//                             $scope.createNoticeMessage = 'Please select Case Number';
//                              }
            }

//            }
//        }


    };

    $scope.reset = function () {
        $scope.notice = angular.copy($scope.master);
        $scope.choiceSet.choices = [];

    };
    /*multi comments end*/

    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();


$scope.reset();

}]);
//****************************createNoticeController**************************************


//****************************caseDepartmentController **************************************
app.controller('caseDepartmentController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("caseDepartmentController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.caseDepartment").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.caseDepartment .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'caseDepartments'}, function (response) {
//        alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;

    $scope.getDepartments = function () {
        $http({
            "method": "GET",
            "url": 'api/caseDepartments',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseDepartmentData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getDepartments();

    $scope.reset = function () {
        $scope.caseDepartment = angular.copy($scope.master);
        $scope.caseCreationError = false;
        $scope.createCaseDepartementError = '';
    };

    $scope.caseDepartmentClick=function(){
        $('#createCaseDepartmentModal').modal('show');
        $scope.caseDepartment = {};
        $scope.caseCreationError = false;
        $scope.createCaseDepartementError = '';
    }

    $scope.caseDepartment = {};
    $scope.depCheck=true;
    $scope.createCaseDepartment = function () {

        $scope.caseCreationError = false;
        $scope.createCaseDepartementError = '';

        if($scope.depCheck==true){
            $scope.depCheck=false;
            if ($scope.caseDepartment.name.match(alphabetsWithSpaces) && $scope.caseDepartment.name) {


                $http({
                    method: 'POST',
                    url: 'api/caseDepartments',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.caseDepartment
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $scope.depCheck=true;
                    $rootScope.caseDepartmentData.push(response);
                    //$scope.caseDepartment = {};
                    $('#createCaseDepartmentModal').modal('hide');
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                    if (response.error.details.messages.name) {
                        $scope.createCaseDepartementError = response.error.details.messages.name[0];
                        $scope.caseCreationError = true;
                        $scope.depCheck=true;
                    }
                });
            }

            else {
                $scope.createCaseDepartementError = 'Please Enter the Case Department Name in the Alphabets';
                $scope.caseCreationError = true;
                $scope.depCheck=true;
            }}
    };
    $scope.updateCaseDepartments = {};

    $scope.editCaseDepartmentPopup = function (caseDepartmentInfo) {

        $scope.updateCaseDepartments = caseDepartmentInfo;
        //console.log("update updateCaseDepartments  " + JSON.stringify($scope.updateCaseDepartments));
    };

    $scope.editCaseDepartments = function () {

        $scope.caseDepartmentUpdationError = false;
        $scope.updateCaseDepartmentError = '';

        if ($scope.updateCaseDepartments.name.match(alphabetsWithSpaces) && $scope.updateCaseDepartments.name) {

            $http({
                method: 'PUT',
                url: 'api/caseDepartments/' + $scope.updateCaseDepartments.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.updateCaseDepartments
            }).success(function (response) {
                //console.log('updateCaseDepartments Response :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $scope.updateCaseDepartments = {};
                $scope.getDepartments();
                $('#caseDepartmentEdit').modal('hide');
            }).error(function (response) {
                console.log('caseDepartmentEdit Error');
                if (response.error.details.messages.name) {
                    $scope.updateCaseDepartmentError = response.error.details.messages.name[0];
                    $scope.caseDepartmentUpdationError = true;
                }
                $timeout(function (){
                    $scope.caseDepartmentUpdationError = false;
                },3000)
            });
        }
        else {
            $scope.updateCaseDepartmentError = 'Please Edit the Court Name in the Correct Format';
            $scope.caseDepartmentUpdationError = true;
            $timeout(function (){
                $scope.caseDepartmentUpdationError = false;
            },3000)
        }

    };

    $scope.cancelEdit = function () {
        $scope.getDepartments();
        $('#caseDepartmentEdit').modal('hide');
    };
    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.caseDepartmentData!=null &&  $scope.caseDepartmentData.length>0){

            $("<tr>" +
                "<th>Case Department Name</th>" +
                "<th>Case Department HeadQuarter</th>" +



                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.caseDepartmentData.length;i++){
                var paymentData=$scope.caseDepartmentData[i];


                $("<tr>" +
                    "<td>"+paymentData.name+"</td>" +
                    "<td>"+paymentData.headQuarter+"</td>" +




                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'caseDepartment');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);
//****************************caseDepartmentController **************************************

//****************************writePetitionController **************************************
app.controller('writePetitionController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("writePetitionController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'writePetition'}, function (response) {
//					alert(JSON.stringify(response));
            		   if(!response){
            			window.location.href = "#/noAccessPage";
            		   }
            		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.getPetition = function () {
        $http({
            "method": "GET",
            "url": 'api/Petitions',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get Petitions   Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.petitionData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getPetition();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.petition = {};


    $scope.getEditWritePetition=function (petitionInfo) {

        //console.log("getEditWritePetition:::" +JSON.stringify(petitionInfo));
        $rootScope.updateWritePetition=petitionInfo;
    }

    if($window.localStorage.getItem('petitionCreationCheck')){
        $scope.caseShow=true;
        $scope.caseHide=false;
        $scope.selectedCaseNumber = $window.localStorage.getItem('petitionCreationCheck')
        $window.localStorage.removeItem('petitionCreationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }

    if($window.localStorage.getItem('petitionUpdationCheck')){
        $scope.caseShow=false;
        $scope.caseHide=true;
        $scope.selectedCaseNumb = $window.localStorage.getItem('petitionUpdationCheck')
        $window.localStorage.removeItem('petitionUpdationCheck')

        $("#addDesignation").modal("hide");
        $('#addDepartmentSuccess').modal('show');
        setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
    }

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.petitionData!=null &&  $scope.petitionData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Petition Id</th>" +
                "<th>Petition Date</th>" +
                "<th>Petitioner</th>" +
                "<th>Respondent</th>" +

                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.petitionData.length;i++){
                var paymentData=$scope.petitionData[i];


                $("<tr>" +
                    "<td>"+paymentData.caseNumber+"</td>" +
                    "<td>"+paymentData.petitionId+"</td>" +
                    "<td>"+paymentData.petitionDate+"</td>" +
                    "<td>"+paymentData.petitioner+"</td>" +
                    "<td>"+paymentData.respondent+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'writePetition');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);
//********************Payment Mode Controller******************


    app.controller('paymentModeController', function ($http, $scope, Upload, $timeout, $window, $location, $rootScope, $q) {
        console.log("paymentModeController Entered...");

        var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
        $scope.getPaymentModes = function () {
            $http({
                "method": "GET",
                "url": 'api/PaymentModes',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                console.log('Payment Get Response :' + JSON.stringify(response));
                $rootScope.paymentData = response;

            }).error(function (response, data) {
                console.log("failure");
            });
        };
        $scope.getPaymentModes();
        $scope.reset = function () {
            $scope.payment = angular.copy($scope.master);
        };
        $scope.payment = {};
        $scope.payCheck=true;
        $scope.createPayment = function () {
            $scope.paymentCreationError = false;
            $scope.createPaymentTypeError = '';

            if($scope.payCheck==true){
                $scope.payCheck=false;

            if($scope.payment.name.match(alphabetsWithSpaces) && $scope.payment.name){
                $http({
                    method: 'POST',
                    url: 'api/PaymentModes',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.payment
                }).success(function (response) {
                    $scope.payCheck=true;
                    $rootScope.paymentData.push(response);
                    $scope.payment = {};
                    $('#myModal').modal('hide');
                }).error(function (response) {
                    console.log('Post Payment Mode Error Response :' + JSON.stringify(response));
                    if (response.error.details.messages.name) {
                        $scope.createPaymentTypeError = response.error.details.messages.name[0];
                        $scope.paymentCreationError = true;
                        $scope.payCheck=true;
                    }

                });
            }else {
                $scope.createPaymentTypeError = 'Please Enter the Payment Mode Name in the Correct Format';
                $scope.paymentCreationError = true;
                $scope.payCheck=true;

            }}

        };
        $scope.updatePayment = {};
        $scope.editPaymentPopup = function (paymentInfo) {
            console.log('Edit Payment:' + JSON.stringify(paymentInfo));
            $scope.updatePayment = paymentInfo;
        };

        $scope.editPayment = function () {
            $scope.paymentUpdationError = false;
            $scope.updatePaymentTypeError = '';

            if($scope.updatePayment.name.match(alphabetsWithSpaces) && $scope.updatePayment.name){
                console.log('Edit Payment 1:' + JSON.stringify($scope.updatePayment));

                $http({
                    method: 'PUT',
                    url: 'api/PaymentModes/' + $scope.updatePayment.id,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.updatePayment
                }).success(function (response) {
                    console.log('updatePayment Response :' + JSON.stringify(response));
                    $scope.updatePayment = {};
                    $scope.getPaymentModes();
                    $('#paymentEdit').modal('hide');
                }).error(function (response) {
                    console.log('paymentUpdate Error');
                    if (response.error.details.messages.name) {
                        $scope.updatePaymentTypeError = response.error.details.messages.name[0];
                        $scope.paymentUpdationError = true;
                    }
                });
            }else {
                $scope.updatePaymentTypeError = 'Please Update the Payment Mode in the Correct Format';
                $scope.paymentUpdationError = true;

            }
        };
        $scope.cancelEdit = function () {
            $scope.getPaymentModes();
            $('#paymentEdit').modal('hide');
        };


        $scope.reset();






    });

//****************************paymentModeController **************************************

//***********************************************case Reports Controller*************************************
app.controller('caseReportsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', 'Excel', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, Excel, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
console.log("caseReportsController Entered");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseReports").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.caseReports .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'caseReports'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

    $scope.getCourt = function(){
    $http({
        method: 'GET',
        url: 'api/Courts',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
//        console.log('courtName  :' + JSON.stringify(response));
        $scope.loadingImage=false;
        $rootScope.courtList = response;

    }).error(function (response) {
        console.log('Error Response :' + JSON.stringify(response));
    });


}
$scope.getCourt();

    $scope.getCaseType = function(){
        $http({
            method: 'GET',
            url: 'api/CaseTypes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('caseTypeList  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseTypeList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getCaseType();

    $scope.getPriority = function(){
        $http({
            method: 'GET',
            url: 'api/Priorities',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('casepriorityList  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.casePriorityList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getPriority();

    $scope.getStatus = function(){
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Statuses  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseStatusList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getStatus();

    $scope.getHearing = function(){
        $http({
            method: 'GET',
            url: 'api/HearingDates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('HearingDates  :' + JSON.stringify(response));
            $scope.loadingImage=false;

            $rootScope.caseHearingList = response;
            var mySet = new Set();
            for(var i=0;i<response.length;i++){
                //alert(JSON.stringify(response[i].editDate));

               mySet.add(response[i].dateHearing);

            }
            $scope.hearingLists = Array.from(mySet);
            console.log(JSON.stringify( $scope.hearingLists));
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getHearing();

    $scope.getPayment = function(){
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('***********AdvocatePayments  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.casePaymentList = response;
//            console.log("00000::"+JSON.stringify($rootScope.casePaymentList))

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getPayment();

    $scope.names = ["Court Wise Report", "Case Type Wise Report","Case Priority Wise Report","Case own/lost/appeals made Report",
        "Case Hearing Date Wise Report","Payments made to Legal Advisor"];

    $scope.selectareas = function (courtName) {
        $scope.selectCourt = courtName;
        //console.log("$scope.selectCourt*******"+JSON.stringify($scope.selectCourt));
        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"courtName":"' + $scope.selectCourt + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
//            console.log('filter for courtName  :' + JSON.stringify(response));
            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCaseDetails = response;
           // console.log("selectCaseDetails*******"+JSON.stringify($scope.selectCaseDetails));


        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });



    }

    $scope.selectCaseTypeareas = function (caseType) {
        $scope.selectcaseType = caseType;
        //console.log("$scope.selectcaseType ::"+JSON.stringify($scope.selectcaseType));

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"caseType":"' + $scope.selectcaseType + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            console.log('filter for caseType  :' + JSON.stringify(response));
            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCaseTypeDetails = response;
            //console.log("$scope.selectCaseTypeDetails*****"+JSON.stringify($scope.selectCaseTypeDetails));

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.selectCasePriorityareas = function (priority) {
        $scope.selectcasePriority = priority;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"priority":"' + $scope.selectcasePriority + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('filter for selectcasePriority  :' + JSON.stringify(response));
            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCasePriorityDetails = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.selectCaseStatusareas = function (status) {
        $scope.selectcaseStatus = status;
        console.log('filter for selectcasestatus  :' + JSON.stringify($scope.selectcaseStatus));
        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"status":"' + $scope.selectcaseStatus + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('filter for selectcasestatus  :' + JSON.stringify(response));
            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCaseStatusDetails = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.selectCaseHearingareas = function (dateHearing) {
        $scope.selectcaseHearing = dateHearing;
        //console.log(":::::::::::"+$scope.selectcaseHearing)
        $http({
            method: 'GET',
            url: 'api/HearingDates/?filter={"where":{"dateHearing":"' + $scope.selectcaseHearing + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('filter for selectcaseHearing  :' + JSON.stringify(response));
            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCaseHearingDetails = response;
            //console.log("selectCaseHearingDetails**********"+JSON.stringify($scope.selectCaseHearingDetails));
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.selectCasePaymentareas = function (name) {
        $scope.selectcasePayment = name;

        $http({
            method: 'GET',
            url: 'api/AdvocatePayments/?filter={"where":{"advocateName":"' + $scope.selectcasePayment + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('filter for selectcasePayment  :' + JSON.stringify(response));
            $scope.showAction=true;
            $scope.loadingImage=false;
            $scope.selectCasePaymentDetails = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }


    $scope.modeChange=function (names) {

        //console.log("paymentModes change" + JSON.stringify(names));

        if(names=="Court Wise Report" )
        {
            $scope.court=true;
            $scope.case=false;
            $scope.priority=false;
            $scope.status=false;
            $scope.hearing=false;
            $scope.payment=false;
            /*alert($scope.court+'...'+$scope.case);*/
        }
       else if(names=="Case Type Wise Report"){
            $scope.priority=false;
            $scope.case=true;
            $scope.court=false;
            $scope.status=false;
            $scope.hearing=false;
            $scope.payment=false;
            }
        else if(names=="Case Priority Wise Report"){
            $scope.priority=true;
            $scope.case=false;
            $scope.court=false;
            $scope.status=false;
            $scope.hearing=false;
            $scope.payment=false;
        }
        else if(names=="Case own/lost/appeals made Report"){
           $scope.status=true;
            $scope.priority=false;
            $scope.case=false;
            $scope.court=false;
            $scope.hearing=false;
            $scope.payment=false;
        }
        else if(names=="Case Hearing Date Wise Report"){
            $scope.status=false;
            $scope.priority=false;
            $scope.case=false;
            $scope.court=false;
            $scope.hearing=true;
            $scope.payment=false;
        }
        else if(names=="Payments made to Legal Advisor"){
            $scope.payment=true;
            $scope.status=false;
            $scope.priority=false;
            $scope.case=false;
            $scope.court=false;
            $scope.hearing=false;
        }

        else {
            $scope.payment=false;
            $scope.court=false;
            $scope.priority=false;
            $scope.case=false;
            $scope.status=false;
            $scope.hearing=false;
        }

    }


    $scope.exportToExcelCourt=function(tableId){
//            alert(tableId);
            if( $scope.selectCaseDetails!=null &&  $scope.selectCaseDetails.length>0){

                $("<tr>" +
                    "<th>Case Id</th>" + "<th>Case Date</th>" + "<th>Case Type</th>" + "<th>Priority</th>" + "<th>Status</th>" + "<th>Court Name</th>" +
                    "<th>Advocate</th>" + "<th>Case Details</th>" +
                    "<th>Act</th>" + "<th>Department</th>" +
                    "<th>Empannel</th>" +
                    "<th>Created Time</th>" +
                    "</tr>").appendTo("table"+tableId);

                for(var i=0;i<$scope.selectCaseDetails.length;i++){
                    var schemes=$scope.selectCaseDetails[i];

                    if(schemes.advocate != null) {
                        $scope.schemeIs= '';
                        for(var j=0; j<schemes.advocate.length;j++) {
                                $scope.schemeIs=$scope.schemeIs + ','+schemes.advocate[j].name;
//                                alert($scope.schemeIs);
                                }
                         }

                            $("<tr>" +
                                "<td>"+schemes.caseNumber+"</td>" +
                                "<td>"+schemes.caseDate+"</td>" +
                                "<td>"+schemes.caseType+"</td>" +
                                "<td>"+schemes.priority+"</td>" +
                                "<td>"+schemes.status+"</td>" +
                                "<td>"+schemes.courtName+"</td>" +
                                "<td>"+$scope.schemeIs+"</td>" +
                                "<td>"+schemes.caseDetails+"</td>" +
                                "<td>"+schemes.act+"</td>" +
                                "<td>"+schemes.department+"</td>" +
                                "<td>"+schemes.empannel+"</td>" +
                                "<td>"+schemes.createdTime+"</td>" +
                                "</tr>").appendTo("table"+tableId);
//                alert("increament")
                }
                var exportHref = Excel.tableToExcel(tableId, 'Court Report Details ');
                $timeout(function () { location.href = exportHref; }, 100);
            } else {
                $("#emptyDataTable").modal("show");
                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
            }

        }

    $scope.exportToExcelCase=function(tableId){
//                alert(tableId);
                if( $scope.selectCaseTypeDetails!=null &&  $scope.selectCaseTypeDetails.length>0){

                    $("<tr>" +
                        "<th>Case Id</th>" +
                        "<th>Case Date</th>" +
                        "<th>Case Type</th>" +
                        "<th>Priority</th>" +
                        "<th>Status</th>" +
                        "<th>Court Name</th>" +
                        "<th>Advocate</th>" +
                        "<th>Act</th>" +
                        "<th>Department</th>" +
                        "<th>Empannel</th>" +
                        "<th>created Time</th>" +
                        "</tr>").appendTo("table"+tableId);

                    for(var i=0;i<$scope.selectCaseTypeDetails.length;i++){
                        var schemes=$scope.selectCaseTypeDetails[i];

                        if(schemes.advocate != null) {
                        $scope.schemeIs= '';
                        for(var j=0; j<schemes.advocate.length;j++) {
                                $scope.schemeIs=$scope.schemeIs + ','+schemes.advocate[j].name;
//                                alert($scope.schemeIs);
                                }
                         }

                        $("<tr>" +
                            "<td>"+schemes.caseNumber+"</td>" +
                            "<td>"+schemes.caseDate+"</td>" +
                            "<td>"+schemes.caseType+"</td>" +
                            "<td>"+schemes.priority+"</td>" +
                            "<td>"+schemes.status+"</td>" +
                            "<td>"+schemes.courtName+"</td>" +
                            "<td>"+$scope.schemeIs+"</td>" +
                            "<td>"+schemes.act+"</td>" +
                            "<td>"+schemes.department+"</td>" +
                            "<td>"+schemes.empannel+"</td>" +
                            "<td>"+schemes.createdTime+"</td>" +
                            "</tr>").appendTo("table"+tableId);
                    }
                    var exportHref = Excel.tableToExcel(tableId, 'Case Type Report Details ');
                    $timeout(function () { location.href = exportHref; }, 100);
                } else {
                    $("#emptyDataTable").modal("show");
                    setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                }

            }

    $scope.exportToExcelPriority=function(tableId){
//                            alert(tableId);
                            if( $scope.selectCasePriorityDetails!=null &&  $scope.selectCasePriorityDetails.length>0){

                                $("<tr>" +
                                    "<th>Case Id</th>" +
                                    "<th>Case Date</th>" +
                                    "<th>Case Type</th>" +
                                    "<th>Priority</th>" +
                                    "<th>Status</th>" +
                                    "<th>Court Name</th>" +
                                    "<th>advocate</th>" +
                                    "<th>Case Details</th>" +
                                    "<th>Act</th>" +
                                    "<th>Department</th>" +
                                    "<th>Empannel</th>" +
                                    "<th>Created Time</th>" +
                                    "</tr>").appendTo("table"+tableId);

                                for(var i=0;i<$scope.selectCasePriorityDetails.length;i++){
                                    var schemes=$scope.selectCasePriorityDetails[i];

                                    if(schemes.advocate != null) {
                                    $scope.schemeIs= '';
                                    for(var j=0; j<schemes.advocate.length;j++) {
                                            $scope.schemeIs=$scope.schemeIs + ','+schemes.advocate[j].name;
                                            }
                                     }

                                    $("<tr>" +
                                        "<td>"+schemes.caseNumber+"</td>" +
                                        "<td>"+schemes.caseDate+"</td>" +
                                        "<td>"+schemes.caseType+"</td>" +
                                        "<td>"+schemes.priority+"</td>" +
                                        "<td>"+schemes.status+"</td>" +
                                        "<td>"+schemes.courtName+"</td>" +
                                        "<td>"+$scope.schemeIs+"</td>" +
                                        "<td>"+schemes.caseDetails+"</td>" +
                                        "<td>"+schemes.act+"</td>" +
                                        "<td>"+schemes.department+"</td>" +
                                        "<td>"+schemes.empannel+"</td>" +
                                        "<td>"+schemes.createdTime+"</td>" +
                                        "</tr>").appendTo("table"+tableId);
                                }
                                var exportHref = Excel.tableToExcel(tableId, 'Priority Report Details ');
                                $timeout(function () { location.href = exportHref; }, 100);
                            } else {
                                $("#emptyDataTable").modal("show");
                                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                            }

                        }

    $scope.exportToExcelStatus=function(tableId){
//                            alert(tableId);
        if( $scope.selectCaseStatusDetails!=null &&  $scope.selectCaseStatusDetails.length>0){

            $("<tr>" +
                "<th>Case Id</th>" +
                "<th>Case Date</th>" +
                "<th>Case Type</th>" +
                "<th>Priority</th>" +
                "<th>Status</th>" +
                "<th>Court Name</th>" +
                "<th>Advocate</th>" +
                "<th>Case Details</th>" +
                "<th>Act</th>" +
                "<th>Department</th>" +
                "<th>Empannel</th>" +
                "<th>Created Time</th>" +
                "</tr>").appendTo("table"+tableId);

                for(var i=0;i<$scope.selectCaseStatusDetails.length;i++){
                    var schemes=$scope.selectCaseStatusDetails[i];

                    if(schemes.advocate != null) {
                    $scope.schemeIs= '';
                    for(var j=0; j<schemes.advocate.length;j++) {
                            $scope.schemeIs=$scope.schemeIs + ','+schemes.advocate[j].name;
                            }
                     }

                $("<tr>" +
                    "<td>"+schemes.caseNumber+"</td>" +
                    "<td>"+schemes.caseDate+"</td>" +
                    "<td>"+schemes.caseType+"</td>" +
                    "<td>"+schemes.priority+"</td>" +
                    "<td>"+schemes.status+"</td>" +
                    "<td>"+schemes.courtName+"</td>" +
                    "<td>"+$scope.schemeIs+"</td>" +
                    "<td>"+schemes.caseDetails+"</td>" +
                    "<td>"+schemes.act+"</td>" +
                    "<td>"+schemes.department+"</td>" +
                    "<td>"+schemes.empannel+"</td>" +
                    "<td>"+schemes.createdTime+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Case Status Wise Report Details ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

    $scope.exportToExcelHearing=function(tableId){
//                            alert(tableId);
                            if( $scope.selectCaseHearingDetails!=null &&  $scope.selectCaseHearingDetails.length>0){

                                $("<tr>" +
                                    "<th>Case Id</th>" +
                                    "<th>Date Hearing</th>" +
                                    "<th>Status</th>" +
                                    "<th>Advocate Name</th>" +
                                    "<th>Comment</th>" +
                                    "<th>Created Time</th>" +
                                    "</tr>").appendTo("table"+tableId);

                                for(var i=0;i<$scope.selectCaseHearingDetails.length;i++){
                                    var schemes=$scope.selectCaseHearingDetails[i];

                                    if(schemes.advocateName != null) {
                                    $scope.schemeIs= '';
                                    for(var j=0; j<schemes.advocateName.length;j++) {
                                            $scope.schemeIs=$scope.schemeIs + ','+schemes.advocateName[j].name;
                                            }
                                     }

                                    $("<tr>" +
                                        "<td>"+schemes.caseNumber+"</td>" +
                                        "<td>"+schemes.dateHearing+"</td>" +
                                        "<td>"+schemes.status+"</td>" +
                                        "<td>"+$scope.schemeIs+"</td>" +
                                        "<td>"+schemes.comment+"</td>" +
                                        "<td>"+schemes.createdTime+"</td>" +
                                        "</tr>").appendTo("table"+tableId);
                                }
                                var exportHref = Excel.tableToExcel(tableId, 'Hearing Report Details ');
                                $timeout(function () { location.href = exportHref; }, 100);
                            } else {
                                $("#emptyDataTable").modal("show");
                                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                            }

                        }

    $scope.exportToExcelPayment=function(tableId){
//                            alert(tableId);
                            if( $scope.selectCasePaymentDetails!=null &&  $scope.selectCasePaymentDetails.length>0){

                                $("<tr>" +
                                    "<th>Case Id</th>" +
                                    "<th>Amount</th>" +
                                    "<th>Mode</th>" +
                                    "<th>Advocate Name</th>" +
                                    "<th>Description</th>" +
                                    "<th>Advocate List</th>" +
                                    "<th>Created Time</th>" +
                                    "</tr>").appendTo("table"+tableId);

                                for(var i=0;i<$scope.selectCasePaymentDetails.length;i++){
                                    var schemes=$scope.selectCasePaymentDetails[i];

                                    if(schemes.advocateList != null) {
                                    $scope.schemeIs= '';
                                    for(var j=0; j<schemes.advocateList.length;j++) {
                                            $scope.schemeIs=$scope.schemeIs + ','+schemes.advocateList[j].name;
                                            }
                                     }

                                    $("<tr>" +
                                        "<td>"+schemes.caseNumber+"</td>" +
                                        "<td>"+schemes.amount+"</td>" +
                                        "<td>"+schemes.mode+"</td>" +
                                        "<td>"+schemes.advocateName+"</td>" +
                                        "<td>"+schemes.description+"</td>" +
                                        "<td>"+$scope.schemeIs+"</td>" +
                                        "<td>"+schemes.date+"</td>" +
                                        "</tr>").appendTo("table"+tableId);
                                }
                                var exportHref = Excel.tableToExcel(tableId, 'Payment Report Details ');
                                $timeout(function () { location.href = exportHref; }, 100);
                            } else {
                                $("#emptyDataTable").modal("show");
                                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                            }

                        }

}]);
//*****************************calendarController***********************************************

//*****************************createWritePetitionController***********************************************
app.controller('createWritePetitionController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
console.log("createWritePetitionController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'writePetition'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();

    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.petition = {};
    $scope.petitionCheck=true;

    console.log("create writepetition function entered");
    $scope.createPetition = function () {
    console.log(":::::::::::::::::::: "+JSON.stringify($scope.user))

        $scope.petitionCreationError = false;
        $scope.createPetitionError = '';

//         if($scope.user.caseId!=undefined){

          if($scope.user.petitionDate){
        if($scope.petitionCheck==true) {
            $scope.petitionCheck = false;

//            var caseNumber = $scope.user.caseId.caseNumber;
//            $scope.user['caseNumber']=caseNumber;
            $http({
                method: 'POST',
                url: 'api/Petitions',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.user
            }).success(function (response) {
                $scope.loadingImage=false;
                $scope.petitionCheck=true;
                $rootScope.petitionData.push(response);
                $scope.selectCaseNumber = response.caseNumber;
                $window.localStorage.setItem('petitionCreationCheck', $scope.selectCaseNumber);
                location.href = '#/writePetition';
            }).error(function (response) {
                console.log('Error Response1 :' + JSON.stringify(response));
                if (response.error.details.messages.petitionId) {
                    $scope.createPetitionError = response.error.details.messages.petitionId[0];
                    $scope.petitionCreationError = true;
                    $scope.petitionCheck=true;
                }
            });
            }
        }else{

        $scope.createPetitionError = 'Please Select Case Petition Date';
         $scope.petitionCreationError = true;
         $scope.petitionCheck=true;
         console.log($scope.petitionCheck)
        }
//        }else{
//        $scope.createPetitionError = 'Please Select Case Number';
//                 $scope.petitionCreationError = true;
//                 $scope.petitionCheck=true;
//        }
    };

$scope.reset();

}]);




//*****************************editWritePetitionController***********************************************//*****************************createWritePetitionController***********************************************

app.controller('editWritePetitionController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
console.log("editWritePetitionController Entered");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'writePetition'}, function (response) {
    //    alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };

    $scope.getCaseNumber();


    //********************************Edit Create Write Petition*********************




    $scope.editWritePetition = function () {
        //console.log('editWritePetition:' + JSON.stringify( $rootScope.updateWritePetition));
        $scope.petitionUpdationError = false;
        $scope.updatePetitionError = '';
 if($rootScope.updateWritePetition.petitionDate){
//  var caseNumber = $scope.updateWritePetition.caseId.caseNumber;
//   $scope.updateWritePetition['caseNumber']=caseNumber;
        $http({
            method: 'PUT',
            url: 'api/Petitions/' +  $rootScope.updateWritePetition.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data:  $rootScope.updateWritePetition
        }).success(function (response) {
            $scope.loadingImage=false;

            $scope.selectCaseNumb=response.caseNumber;
            $window.localStorage.setItem('petitionUpdationCheck',$scope.selectCaseNumb);
            location.href = '#/writePetition';
            $scope.getCaseNumber();

        }).error(function (response) {
            console.log('caseUpdate Error');

            if (response.error.details.messages.petitionId) {
                $scope.updatePetitionError = response.error.details.messages.petitionId[0];
               $scope.petitionUpdationError = true;
           }
        });

          }else{
          $scope.petitionUpdationError = true;
          $scope.updatePetitionError = 'Please Select Case Petition Date';
          }

    };


//***********edit Write Petition controller


}]);
//*****************************editWritePetitionController***********************************************

//*******************************petitionDetailsController************************************
app.controller('petitionDetailsController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {
    console.log("petitionDetailsController Entered");



    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.writePetition .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'writePetition'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;

    $scope.generatePDF = function() {
            var printContents = document.getElementById('formConfirmation').innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;

        }
    $scope.getPetitionId = function () {
        $http({
            method: 'GET',
            url: 'api/Petitions',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('petitionId  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.petitionList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getPetitionId();


    $scope.selectareas = function (petitionId) {
        $scope.selectPetitionId = petitionId;

        $http({
            method: 'GET',
            url: 'api/Petitions/?filter={"where":{"petitionId":"' + $scope.selectPetitionId + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter for selectPetitionId  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectPetitionDetails = response[0];

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });



    }
}]);
//*******************************petitionDetailsController************************************
//*******************************empannelGroupController************************************
app.controller('empannelGroupController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder' ,function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("EmpannelGroupController Entered");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #legalManagmentLi").addClass("active");
            $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
            $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
            $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.empannelGroup").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.empannelGroup .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'empannelGroup'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.loadingImage=true;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    var alphaNumeric =/^[a-z\d\-_\s]+$/i;
    $scope.getEmpannel = function () {
        $http({
            "method": "GET",
            "url": 'api/EmpannelGroups',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('EmpannelGroups Get Response :' + JSON.stringify(response));
            $rootScope.empannelData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getEmpannel();
    $scope.reset = function () {
        $scope.empannel = angular.copy($scope.master);
        $scope.empannelCreationError = false;
        $scope.createEmpannelError = '';
    };
    $scope.empanelClick=function(){
        $('#createEmpanelModal').modal('show');
        $scope.empannel = {};
        $scope.empannelCreationError = false;
        $scope.createEmpannelError = '';
    }

    $scope.empannel = {};
    $scope.empCheck=true;
    $scope.createEmpannel = function () {
               console.log("================"+JSON.stringify($scope.empannel));

        $scope.advocateData = [];
        for(var i=0;i<$scope.empannel.advocateName.length;i++){
            $scope.advocateData.push(JSON.parse($scope.empannel.advocateName[i]));

        }
        $scope.empannel.advocateName=[];
        for(var i=0;i<$scope.advocateData.length;i++){

            //$scope.empannel.advocateName.push({
            //    name:$scope.advocateData[i].name,
            //    email:$scope.advocateData[i].email
            //});

            $scope.empannel.advocateName.push($scope.advocateData[i].advId);

        }


        $scope.empannelCreationError = false;
        $scope.createEmpannelError = '';

        if($scope.empCheck==true){
            $scope.empCheck=false;
            if ($scope.empannel.groupName.match(alphabetsWithSpaces) && $scope.empannel.groupName) {

            console.log("================"+JSON.stringify($scope.empannel));
            $http({
                method: 'POST',
                url: 'api/EmpannelGroups',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.empannel
            }).success(function (response) {
                $scope.loadingImage=false;
                //console.log(JSON.stringify(response));
                $scope.empCheck=true;
                $rootScope.empannelData.push(response);
                //$scope.empannel = {};
                $('#createEmpanelModal').modal('hide');
                //$rootScope.empannelData();
                $window.location.reload();
            }).error(function (response) {
                console.log('Post EmpannelGroups Error Response :' + JSON.stringify(response));
                if (response.error.details.messages.groupName) {
                    $scope.createEmpannelError = response.error.details.messages.groupName[0];
                    $scope.empannelCreationError = true;
                    $scope.empCheck=true;
                }
            });
        }else{
            $scope.createEmpannelError = 'Please Enter the Group Name in the Correct Format';
            $scope.empannelCreationError = true;
                $scope.empCheck=true;

        }}
    };
    $scope.updateEmpannel = {};
    $scope.editEmpannelPopup = function (empannelInfo) {
        //console.log('Edit updateEmpannel:' + JSON.stringify(empannelInfo));
        $scope.updateEmpannel = empannelInfo;
        console.log("EEEEE :"+JSON.stringify(empannelInfo));
        //$('#empannelEdit').modal('show');
    };
    $scope.editEmpannel = function () {



        $scope.empannelUpdationError = false;
        $scope.updateEmpannelError = '';

        $scope.advocateData = [];
        for(var i=0;i<$scope.updateEmpannel.advocateName.length;i++){
            $scope.advocateData.push(JSON.parse($scope.updateEmpannel.advocateName[i]));
            //alert("2780"+JSON.stringify($scope.advocateData));
        }
        $scope.updateEmpannel.advocateName = [];

        for(var i=0;i<$scope.advocateData.length;i++){
            $scope.updateEmpannel.advocateName.push($scope.advocateData[i].advId);
            //$scope.updateEmpannel.advocateName.push({
            //    name:$scope.advocateData[i].name,
            //    email:$scope.advocateData[i].email
            //});
            //
        }
        //console.log('AAAAAAAAAAAAAAAAA:' + JSON.stringify($scope.updateEmpannel));

         //if($scope.updateEmpannel) {
         //    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
         //            delete  $scope.updateEmpannel["$$hashKey"]
         //}
        if ($scope.updateEmpannel.groupName.match(alphabetsWithSpaces) && $scope.updateEmpannel.groupName) {
            //console.log('Edit empannel data:' + JSON.stringify($scope.updateEmpannel));
               if($scope.updateEmpannel.advocateList){
                   delete  $scope.updateEmpannel.advocateList["$$hashKey"]
                    //console.log('Edit empannel data:' + JSON.stringify($scope.updateEmpannel));
               }
              var updateEmpannel={
                               "groupName":$scope.updateEmpannel.groupName,
                  "advocateName":$scope.updateEmpannel.advocateName

              }


            $http({
                method: 'PUT',
                url: 'api/EmpannelGroups/' + $scope.updateEmpannel.id,
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: updateEmpannel
            }).success(function (response) {
                $scope.loadingImage=false;
                //console.log('updateEmpannel Response :' + JSON.stringify(response));
                //$scope.updateEmpannel = {};
                $scope.getEmpannel();
                $('#empannelEdit').modal('hide');
                $rootScope.empannelData();
            }).error(function (response) {
                console.log('updateEmpannel Error'+JSON.stringify(response));
                if (response.error.details.messages.groupName) {
                    $scope.updateEmpannelError = response.error.details.messages.groupName[0];
                    $scope.empannelUpdationError = true;
                }
                $timeout(function (){
                    $scope.empannelUpdationError = false;
                },3000)
            });
        }else{
            $scope.updateEmpannelError = 'Please Enter The Group Name in the Correct Format';
            $scope.empannelUpdationError = true;
            $timeout(function (){
                $scope.empannelUpdationError = false;
            },3000)
        }

    };
    $scope.cancelEdit = function () {
        $scope.getEmpannel();
        $('#empannelEdit').modal('hide');
    };



    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getAdvocates Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.advocates = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getAdvocates();

    //$scope.checkAdvocateNameSelected = function(advocateName){
    //
    //        console.log('......................' + JSON.stringify($scope.updateEmpannel));
    //        var check;
    //        for (var i = 0; i < $scope.updateEmpannel.advocateName.length; i++) {
    //
    //            if ($scope.updateEmpannel.advocateName[i].email == advocateName.email) {
    //                console.log("4383" + JSON.stringify(advocateName));
    //                check = true;
    //                break;
    //            }
    //        }
    //
    //        if (check) {
    //            return true;
    //        } else {
    //            return false;
    //        }
    //
    //};

    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.empannelData!=null &&  $scope.empannelData.length>0){

            $("<tr>" +
                "<th>Group Name</th>" +
                "<th>Advocate Name</th>" +


                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.empannelData.length;i++){
                var paymentData=$scope.empannelData[i];


                $("<tr>" +
                    "<td>"+paymentData.groupName+"</td>" +
                    "<td>"+paymentData.advocateList[0].name+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'empanelGroup');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

}]);

app.controller('judgementController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$timeout', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope,$timeout,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("judgementController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.judgement").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.judgement .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'judgement'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.getJudgementDetails = function () {
        $http({
            "method": "GET",
            "url": 'api/Judgements',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('judgementController Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.judgementData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getJudgementDetails();
    $scope.reset = function () {
        $scope.judgement = angular.copy($scope.master);
        $scope.judgementErrorMessage="";
        $scope.judgementError=false;
    };
    $scope.judgementClick=function(){
        $('#judgementModal').modal('show');
        $scope.judgement = {};
        $scope.judgementErrorMessage="";
        $scope.judgementError=false;

    }
    $scope.judgement = {};
    $scope.judgementCheck=true;
    $scope.createJudgement = function () {
        $scope.judgementErrorMessage="";
        $scope.judgementError=false;
        if($scope.judgement.caseId !=undefined){
        if($scope.judgement.courts!=undefined){
        if($scope.judgement.statuses!=undefined){
        if($scope.judgementCheck==true) {
            $scope.judgementCheck = false;
           if($scope.judgement.judgementDate){

            var caseNumber = $scope.judgement.caseId.caseNumber;
           var status = $scope.judgement.statuses.field1;
           var court = $scope.judgement.courts.name;
           $scope.judgement['caseNumber']=caseNumber;
           $scope.judgement['status']=status;
           $scope.judgement['court']=court;

            $http({
                method: 'POST',
                url: 'api/Judgements',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.judgement
            }).success(function (response) {
                //console.log('judgement Post Response :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $rootScope.judgementData.push(response);
                $scope.selectCaseNumber = response.caseNumber;
                $scope.caseShow = true;
                $scope.caseHide = false;
                $scope.judgementCheck=false;
                $("#addDesignation").modal("hide");
                $('#addDepartmentSuccess').modal('show');
                setTimeout(function () {
                    $('#addDepartmentSuccess').modal('hide')
                }, 9000);

                $('#judgementModal').modal('hide');
            }).error(function (response) {
                console.log('Post Judgement Error Response :' + JSON.stringify(response));
                if(response.error.message){
                $scope.judgementErrorMessage=response.error.message;
                               $scope.judgementError=true;
                               $scope.judgementCheck=true;
                }

                $scope.judgementCheck=true;
            });
        }else{

               $scope.judgementErrorMessage="Please Select Judgement Date";
               $scope.judgementError=true;
               $scope.judgementCheck=true;
           }}
           }else{

          $scope.judgementError = true;
           $scope.judgementErrorMessage = 'Please Select Status';
           $scope.judgementCheck=true;

             }
        }else{

           $scope.judgementError = true;
            $scope.judgementErrorMessage = 'Please Select Court';
            $scope.judgementCheck=true;

          }

          }else{

         $scope.judgementError = true;
          $scope.judgementErrorMessage = 'Please Select Case Number';
          $scope.judgementCheck=true;

            }
    };
    $scope.updateJudgement = {};
    $scope.editJudgementPopup = function (judgementInfo) {
       // console.log('Edit judgementInfo:' + JSON.stringify(judgementInfo));
        $scope.updateJudgement = judgementInfo;
    };

    $scope.editJudgement = function () {
        $scope.judgementUpdateErrorMessage="";
        $scope.judgementUpdateError=false;
        if($scope.updateJudgement.judgementDate){

        var caseNumber = $scope.updateJudgement.caseId.caseNumber;
                   var status = $scope.updateJudgement.statuses.field1;
                   var court = $scope.updateJudgement.courts.name;
                   $scope.updateJudgement['caseNumber']=caseNumber;
                   $scope.updateJudgement['status']=status;
                   $scope.updateJudgement['court']=court;

        $http({

            method: 'PUT',
            url: 'api/Judgements/' + $scope.updateJudgement.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $scope.updateJudgement
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.selectCaseNumb=response.caseNumber;
            $scope.caseShow=false;
            $scope.caseHide=true;
            $("#addDesignation").modal("hide");
            $('#addDepartmentSuccess').modal('show');
            setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            $('#judgementEdit').modal('hide');

        }).error(function (response) {
            console.log('Judgements Error'+JSON.stringify(response));
        });
        }else{
            $scope.judgementUpdateErrorMessage="Please select Judgement Date";
            $scope.judgementUpdateError=true;
        }
    };
    $scope.cancelEdit = function () {
        $scope.getJudgementDetails();
        $('#judgementEdit').modal('hide');
    };
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();

    $scope.getCourts = function () {
        $http({
            "method": "GET",
            "url": 'api/Courts',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.courtData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getCourts();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get status Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.status = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };

    $scope.getStatus();

    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.judgementData!=null &&  $scope.judgementData.length>0){

            $("<tr>" +
                "<th>Case Number</th>" +
                "<th>Court Name</th>" +
                "<th>Judgement Date</th>" +
                "<th>Status</th>" +
                "<th>Judgement Details</th>" +

                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.judgementData.length;i++){
                var paymentData=$scope.judgementData[i];


                $("<tr>" +
                    "<td>"+paymentData.caseNumber+"</td>" +
                    "<td>"+paymentData.court+"</td>" +
                    "<td>"+paymentData.judgementDate+"</td>" +
                    "<td>"+paymentData.status+"</td>" +
                    "<td>"+paymentData.judgementDetails+"</td>" +


                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'judgement');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }
}]);


//*******************************empannelGroupController************************************

//*******************************caseHistoryController************************************
app.controller('caseHistoryController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', '$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder',function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,$routeParams, DTOptionsBuilder, DTColumnBuilder) {
    console.log("caseHistoryController Entered");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.case .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

     var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                    		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'createCase'}, function (response) {
    //    					alert(JSON.stringify(response));
                    		   if(!response){
                    			window.location.href = "#/noAccessPage";
                    		   }
                    		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var btn = $('#expandAll,#collapseAll').click(function() { // bind click handler to both button
        $(this).hide(); // hide the clicked button
        btn.not(this).show(); // show the another button which is hidden
    });

    $(function () {
        $('a[data-toggle="collapse"]').on('click',function(){

            var objectID=$(this).attr('href');

            if($(objectID).hasClass('in'))
            {
                $(objectID).collapse('hide');
            }

            else{
                $(objectID).collapse('show');
            }
        });


        $('#expandAll').on('click',function(){
            $('a[data-toggle="collapse"]').each(function(){
                var objectID=$(this).attr('data-target');
                if($(objectID).hasClass('in')===false)
                {
                    $(objectID).collapse('show');
                }
            });
        });

        $('#collapseAll').on('click',function(){
            $('a[data-toggle="collapse"]').each(function(){
                var objectID=$(this).attr('data-target');
                $(objectID).collapse('hide');
            });
        });

    });
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('CaseNumber  :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.caseList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });


    }
    $scope.getCaseNumber();


    //******file Upload***********
    var formIdsArray = [];
    var formUploadStatus = false;
    $scope.errorMssg1 = false;
    $scope.fileUploadSuccess = false;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        //var count=1;
        //  filedetails=[];
        formIdsArray = [];
        //$scope.docList = [];
        $scope.errorMssg1 = true;
        $scope.fileUploadSuccess = false;
        //$scope.forms = files;
        var fileCount = 0;

        angular.forEach(files, function (file) {
            $scope.errorMssg1 = true;
            $scope.fileUploadSuccess = false;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {

                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.errorMssg1 = false;
                $scope.fileUploadSuccess = true;
                $scope.uploadError = false;
                $scope.uploadErrorMessage = '';
            }
        });
    };
//******file Upload***********

    $scope.reset=function () {
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;

    }
    $scope.cancel=function(){
        $scope.upload={};
        $scope.docList=[];
        $scope.fileUploadSuccess = false;
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").hide();
            ;
        });
    }
    $scope.uploadError = false;
    $scope.uploadErrorMessage = '';
    $scope.caseUploads = function () {
        // //alert('hai');
        if (formUploadStatus) {
            if ($scope.upload && $scope.upload.comments) {
                var uploadDetails = $scope.upload;
                uploadDetails.file = $scope.docList;
                var caseNumber = $scope.selectCaseNumber;

                uploadDetails['caseNumber'] = caseNumber;
                var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
                uploadDetails['createdPerson'] = loginPersonDetails.name;
                uploadDetails['uploadedPersonName'] = loginPersonDetails.name;
                //  //alert('uplaoded data is'+JSON.stringify($scope.docList));
                uploadDetails.status = 'active';
                $http({
                    method: 'POST',
                    url: 'api/CaseUploads',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": uploadDetails
                }).success(function (response) {
                    $scope.loadingImage=false;
                    //console.log('Users Response :' + JSON.stringify(response));
                    $("#scroll2").hide();
                    $("#scroll").show();
                    $scope.errorMssg1 = false;
                    //$scope.fileUploadSuccess = false;
                    $scope.uploadError = false;
                    $scope.uploadErrorMessage = '';
                    $scope.upload={};
                    $scope.docList = [];
                    $scope.getDocuments();
                }).error(function (response) {
                    //console.log('Error Response :' + JSON.stringify(response));
                });
                //console.log('scheme details are'+JSON.stringify(schemeDetails));
            }else{
                $scope.uploadError = true;
                $scope.uploadErrorMessage = 'Please enter the comment';
            }
        }else {
            //$scope.createScheme();
            $scope.uploadError = true;
            $scope.uploadErrorMessage = 'Please attach the files.';
        }
    }

    $scope.docUploadURL = uploadFileURL;

    $scope.getDocuments = function () {
        console.log("case number"+$scope.selectCaseNumber);
        $http({
            method: 'GET',
            url: 'api/CaseUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.documentsList = response;

            //console.log(" $scope.documentsList"+ JSON.stringify($scope.documentsList));
            //console.log(" $scope.documentsList"+ JSON.stringify(response));
            //   //alert('hai'+response.length);
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }
    //***************************HearingDateDocuments**************************

    $scope.getHearingDocuments = function () {

        $http({
            method: 'GET',
            url: 'api/HearingUploads/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.hearingDocumentsList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }
    //***************************HearingDateDocuments**************************


    //***********select*************

    $scope.selectareas = function (caseNumber) {
        $scope.selectCaseNumber = caseNumber;

        $http({
            method: 'GET',
            url: 'api/CreateCases/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter for case number :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectCaseDetails = response[0];

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

        $scope.selectareas22(caseNumber);
        $scope.selectHearingDetails(caseNumber);
        $scope.selectHearingHistory(caseNumber);
        $scope.selectJudgementDetails(caseNumber);
        $scope.selectJudgementHistory(caseNumber);
        $scope.getDocuments();
        $scope.getHearingDocuments();



    }


    $(function () {
        $("#scroll2").hide();
        $("#scroll").click(function () {
            $("#scroll2").show();
            $("#scroll").hide();
            $('html,body').animate({
                    scrollTop: $("#scroll2").offset().top
                },
                'slow');
        });
        $("#cancel").click(function () {
            $("#scroll2").hide()
            $("#scroll").show();
            ;
        });


    })

    //*****************************case History**************************

    $scope.selectareas22 = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/CaseHistories/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter selectCaseHistory case number :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectCaseHistory = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

        $scope.getDocuments();

    }

    $scope.selectHearingDetails = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/HearingDates/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter selectHearingDetails :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectHearingHistory = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }
    $scope.selectHearingHistory = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/HearingHistories/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter selectHearingDetails :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectHearingHistoryDetails = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }

    $scope.selectJudgementDetails = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/Judgements/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter selectHearingDetails :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.selectJudgements = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }
    $scope.selectJudgementHistory = function (caseNumber) {

        $http({
            method: 'GET',
            url: 'api/JudgementHistories/?filter={"where":{"caseNumber":"' + $scope.selectCaseNumber + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('filter selectHearingDetails :' + JSON.stringify(response));
            $scope.selectJudgementHistory = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });

    }
    $scope.reset();

}]);
//*******************************caseHistoryController************************************
// *******************************hearingCaseDetailsController************************************

app.controller('hearingCaseDetailsController', function ($http, $scope, Upload, $timeout, $window, $location, $rootScope, $q) {


});
// *******************************hearingCaseDetailsController************************************
// *******************************legalEmailTemplateController************************************

app.controller('legalEmailTemplateController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.legalEmailTemplate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.legalEmailTemplate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'legalEmailTemplate'}, function (response) {
    //alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.editEmailTemp = function() {

        var editEmailData= $scope.legalType;
        //var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        //editEmailData.lastEditPerson=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/EmailTempletes/'+$scope.legalType.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editEmailData
        }).success(function (response, data) {
            $scope.loadingImage=false;
            //console.log("filter EmailTempletes "+ JSON.stringify(response));
            $scope.emailAlert = response;
            //$window.location.reload();
           // console.log("filter EmailTempletes "+ JSON.stringify($scope.emailAlert));
            $("#registerSuccess").modal("show");
            setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
            /*$scope.getEmail();*/
        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.getEmail=function () {

        $http({
            method: 'GET',
            url: 'api/EmailTempletes?filter={"where":{"emailType":"legal"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.legalType = response[0];
            //console.log("$scope.email$scope.email:::"+JSON.stringify($scope.legalType));
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getEmail();


}]);
// *******************************legalEmailTemplateController************************************

// *******************************createHearingDateController************************************
app.controller('createHearingDateController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Upload', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(legalManagement, $http, $scope, $window, $location, $rootScope, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("createHearingDateController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'hearingDateInfo'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.gethearingDate = function () {
        $http({
            "method": "GET",
            "url": 'api/HearingDates',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('*****hearingDateController Get Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.hearingData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.gethearingDate();

    $scope.hearingDateClick=function(){
        //$('#createHearingDateModal').modal('show');
        $scope.hearing = {};

    }
    $scope.hearing = {};
    $scope.hearingCheck=true;
    $scope.createHearingDate = function () {
    $scope.createHearingMessage ='';
    $scope.hearingError = false;
        //alert(JSON.stringify($scope.hearing.advocateName));

        $scope.advocateData = [];

        for(var i=0;i<$scope.hearing.advocateName.length;i++){
            $scope.advocateData.push(JSON.parse($scope.hearing.advocateName[i]));
        }
        $scope.hearing.advocateName = [];

        for(var i=0;i<$scope.advocateData.length;i++){

            $scope.hearing.advocateName.push({
               name:$scope.advocateData[i].name,
                               email:$scope.advocateData[i].email,
                               advId:$scope.advocateData[i].advId,
                               phoneNumber:$scope.advocateData[i].phoneNumber
            });

//$scope.hearing.advocateName.push($scope.advocateData[i].advId);
        }

        console.log("--------==--------- "+JSON.stringify($scope.hearing))
//                  if($scope.hearing.caseDetails!=undefined){
//                  if($scope.hearing.statuses!=undefined){

        if($scope.hearingCheck==true) {
            $scope.hearingCheck = false;

            var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
            $scope.hearing['lastEditPerson'] = loginPersonDetails.employeeId;


//          var caseNumber=$scope.hearing.caseDetails.caseNumber;
//          var status=$scope.hearing.statuses.status;
//          $scope.hearing['caseNumber']=caseNumber;
//          $scope.hearing['status']=status;

            $http({
                method: 'POST',
                url: 'api/HearingDates',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.hearing
            }).success(function (response) {
                //console.log('hearing Post Response :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $rootScope.hearingData.push(response);
                $scope.hearingCheck = true;
                $scope.selectCaseNumber = response.caseNumber;
                $scope.caseShow = true;
                $scope.caseHide = false;


                $scope.selectCaseNumber = response.caseNumber;
                $window.localStorage.setItem('hearingCreationCheck', $scope.selectCaseNumber);
                location.href = '#/hearingDate';
//                $window.location.reload();
               //$('#createHearingDateModal').modal('hide');
            }).error(function (response) {
                console.log('Post hearing Error Response :' + JSON.stringify(response));

                  if (response.error.message) {
                  $scope.createHearingMessage = response.error.message;
                  $scope.hearingError = true;

                 $scope.hearingCheck = true;
                 }


            });
}
//            }else{
//                $scope.createHearingMessage = 'Please select Status';
//                  $scope.hearingError = true;
//
//                 $scope.hearingCheck = true;
//                }
//            }else{
//                $scope.createHearingMessage = 'Please select Case Number';
//                  $scope.hearingError = true;
//
//                 $scope.hearingCheck = true;
//            }

    };

    $scope.reset = function () {
        $scope.hearing = angular.copy($scope.master);
         $scope.createHearingMessage ='';
            $scope.hearingError = false;
    };
    $scope.reset();

    //*************get CaseNumber*******
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.caseNumber = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getAdvocates Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.advocates = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getAdvocates();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('getStatus Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.status = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getStatus();

}]);
// *******************************createHearingDateController************************************
// *******************************editHearingDateController************************************
app.controller('editHearingDateController', ['legalManagement', '$http', '$scope', '$window', '$location', '$rootScope', function(legalManagement, $http, $scope, $window, $location, $rootScope) {
    console.log("editHearingDateController Entered...");

$(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalCasePro.active #legalCaseProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalCasePro.active #legalCaseProSetUp li.hearingDate .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });


    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                		legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'hearingDateInfo'}, function (response) {
//    					alert(JSON.stringify(response));
                		   if(!response){
                			window.location.href = "#/noAccessPage";
                		   }
                		});

    $scope.gethearingDate = function () {
        $http({
            "method": "GET",
            "url": 'api/HearingDates',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('*****hearingDateController Get Response :' + JSON.stringify(response));
            $rootScope.hearingData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.gethearingDate();


    $scope.hearingCheck=true;

    $scope.updateHearingDate = {};
    //$scope.edithearingPopup = function (hearingInfo) {
    //    console.log('Edit hearing:' + JSON.stringify(hearingInfo));
    //    $scope.updateHearingDate = hearingInfo;
    //};

    $scope.editHearingDate = function () {
        alert('Edit hearing1');
        console.log('Edit hearing1:' + JSON.stringify($scope.updateHearingDate));
        $scope.advocateData = [];

        for(var i=0;i<$scope.updateHearingDate.advocateName.length;i++){
            $scope.advocateData.push(JSON.parse($scope.updateHearingDate.advocateName[i]));
        }
        $scope.updateHearingDate.advocateName = [];

        for(var i=0;i<$scope.advocateData.length;i++){
            //alert("in for");
            $scope.updateHearingDate.advocateName.push({
                name:$scope.advocateData[i].name,
                email:$scope.advocateData[i].email
            });

        }

        $http({
            method: 'PUT',
            url: 'api/HearingDates/' + $scope.updateHearingDate.id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: $rootScope.updateHearingDate
        }).success(function (response) {
            console.log('hearing put Response :' + JSON.stringify(response));
            $scope.selectCaseNumb=response.caseNumber;
            //$scope.caseShow=false;
            //$scope.caseHide=true;
            //$('#paymentEdit').modal('hide');
            //$("#addDesignation").modal("hide");
            //$('#addDepartmentSuccess').modal('show');
            //setTimeout(function(){$('#addDepartmentSuccess').modal('hide')}, 9000);
            //$('#hearingEdit').modal('hide');
            $scope.selectCaseNumb=response.caseNumber;
            $window.localStorage.setItem('hearingUpdationCheck', $scope.selectCaseNumb);
            location.href = '#/hearingDate';
        }).error(function (response) {
            console.log('hearingDate Error');
        });

    };
    $scope.cancelEdit = function () {
        $scope.gethearingDate();
        //$('#hearingEdit').modal('hide');
    };
    $scope.reset();

    //*************get CaseNumber*******
    $scope.getCaseNumber = function () {
        $http({
            method: 'GET',
            url: 'api/CreateCases',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('get creasecase Response :' + JSON.stringify(response));
            $scope.caseNumber = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getCaseNumber();

    $scope.getAdvocates = function () {
        $http({
            method: 'GET',
            url: 'api/Advocates',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('getAdvocates Response :' + JSON.stringify(response));
            $scope.advocates = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getAdvocates();

    $scope.getStatus = function () {
        $http({
            method: 'GET',
            url: 'api/Statuses',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('getStatus Response :' + JSON.stringify(response));
            $scope.status = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getStatus();
    //$scope.checkAdvocateHearingSelected = function(advocate){
    //    var check;
    //    for(var i=0; i < $rootScope.updateHearingDate.advocate.length; i++) {
    //        if ($rootScope.updateHearingDate.advocate[i].email == advocate.email) {
    //            check = true;
    //            break;
    //        }
    //    }
    //
    //    if(check){
    //        return true;
    //    }else{
    //        return false;
    //    }
    //};


}]);
// *******************************editHearingDateController************************************

//***********Holiday Controller*********************
app.controller('holidaysController', ['legalManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$q', 'Excel', 'DTOptionsBuilder', 'DTColumnBuilder' ,function (legalManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $q,Excel, DTOptionsBuilder, DTColumnBuilder) {
    console.log("holidaysController Entered...");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #legalManagmentLi").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro").addClass("active");
        $(".sidebar-menu #legalManagmentLi.treeview #legalMasterPro.active #legalMasterProSetUp").addClass("menu-open");
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.holidays").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #legalWorks #legalMasterPro.active #legalMasterProSetUp li.holidays .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    legalManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'holidays'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
    $scope.getHolidays = function () {
        $http({
            "method": "GET",
            "url": 'api/Holidays',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $rootScope.holidaysData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getHolidays();
    $scope.reset = function () {
        $scope.holiday = angular.copy($scope.master);
        $scope.holidayCreationError = false;
        $scope.createHolidayError = '';

    };

    $scope.holidaysClick=function(){
        $('#CreateHolidayModel').modal('show');
        $scope.holiday = {};
        $scope.holidayCreationError = false;
        $scope.createHolidayError = '';

        $scope.holidayUpdationError = false;
        $scope.updateHolidayError = '';
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

    $scope.holiday = {};
    $scope.holidayCheck=true;
    $scope.createHoliday = function () {
        $scope.holidayCreationError = false;
        $scope.createHolidayError = '';
        if(  $scope.holidayCheck==true) {
            $scope.holidayCheck=false;
            if ($scope.holiday.festival.match(alphabetsWithSpaces) && $scope.holiday.festival) {
//                console.log("posttttttttttttttt")

                $http({
                    method: 'POST',
                    url: 'api/Holidays',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.holiday
                }).success(function (response) {

                    $scope.loadingImage=false;
                    $scope.holidayCheck=true;
                    //$scope.holidaysData.push(response);
                    //$scope.case = {};
                    $('#CreateHolidayModel').modal('hide');
                    $scope.getHolidays();
//                    $window.location.reload();
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                    if (response.error.message[0]) {
                        $scope.createHolidayError = response.error.message[0];
                        $scope.holidayCreationError = true;
                        $scope.holidayCheck=true;
                    }

                });
            } else {
                $scope.createHolidayError = 'Please Enter Alphabetic Characters Only';
                $scope.holidayCreationError = true;
                $scope.holidayCheck=true;
            }
        }

    };
    $scope.updateHoliday = {};
    $scope.editHolidayPopup = function (holidayInfo) {

        $scope.updateHoliday=angular.copy(holidayInfo);
//        $scope.updateHoliday = holidayInfo;
        console.log("update updateHoliday " + JSON.stringify($scope.updateHoliday));
    };
    $scope.holidayUpdateCheck=true;

    $scope.editHoliday = function () {
        $scope.holidayUpdationError = false;
        $scope.updateHolidayError = '';


        if($scope.holidayUpdateCheck==true) {
            $scope.holidayUpdateCheck = false;
            if ($scope.updateHoliday.festival.match(alphabetsWithSpaces) && $scope.updateHoliday.festival) {


                $http({
                    method: 'PUT',
                    url: 'api/Holidays/' + $scope.updateHoliday.id,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.updateHoliday
                }).success(function (response) {

                    $scope.loadingImage=false;
                    $scope.holidayUpdateCheck = true;
                    $scope.updateHoliday = {};
                    $scope.getHolidays();
                    $('#holidayEdit').modal('hide');
//                    $window.location.reload();
                }).error(function (response) {
                    console.log('holidayEdit Error');
                    console.log(JSON.stringify(response));
                    //if (response.error.details.messages.festival) {
                    //    $scope.updateHolidayError = response.error.details.messages.festival[0];
                    //    $scope.holidayUpdationError = true;
                    //    $scope.holidayUpdateCheck = true;
                    //}
                    $timeout(function (){
                        $scope.holidayUpdationError = false;
                    },3000)
                });
            } else {
                $scope.updateHolidayError = 'Please Enter Alphabetic Characters Only';
                $scope.holidayUpdationError = true;
                $scope.holidayUpdateCheck = true;

                $timeout(function (){
                    $scope.holidayUpdationError = false;
                },3000)

            }
            //} else {
            //    $scope.updateCaseTypeError = 'Please enter the Case Number in the Correct format';
            //    $scope.caseTYpeUpdationError = true;
            //
            //}
        }
    };
    $scope.cancelEdit = function () {
        $scope.getHolidays();
        $('#holidayEdit').modal('hide');
    };

    $scope.reset();

    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.holidaysData!=null &&  $scope.holidaysData.length>0){

            $("<tr>" +
                "<th>Festival Name</th>" +
                "<th>Festival Description</th>" +
                "<th>Festival Date</th>" +
                "</tr>").appendTo("table"+tableId);
            for(var i=0;i<$scope.holidaysData.length;i++){
                var holidays=$scope.holidaysData[i];
                $("<tr>" +
                    "<td>"+holidays.festival+"</td>" +
                    "<td>"+holidays.description+"</td>" +
                    "<td>"+holidays.festivalDate+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'List Of Holidays');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);
//***********Holiday Controller*********************

function checkMailFormat(checkData) {
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    console.log("checkData" +checkData);
    if (checkData.match(mailFormat)) {
        return true;
    } else {

        return false;
    }
}

function checkPhoneNumber(checkData) {
    var phoneNumber = /^[7-9]{1}[0-9]{9}$/;
    if (checkData.match(phoneNumber)) {
        return true;
    }
    else {
        return false;
    }
}

function checkOnlyAlphabets(checkData) {
    var onlyAlphabets = /^[A-Za-z]+$/;
    if (checkData.match(onlyAlphabets)) {
        return true;
    } else {
        ////alert('Entered');
        return false;
    }
}

function checkAlphaNumeric(checkData) {
    var alphaNumeric = /^[a-z\d\-_\s]+$/i;
    if (checkData.match(alphaNumeric)) {
        return true;
    } else {
        return false;
    }

}

app.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
})